#pragma once
#include<memory>

#include "model.h"
#include "model_renderer.h"
#include"Geometric_Primitive.h"

using namespace DirectX;
struct Parameter_Data
{
	float Jpower; //ジャンプパワー
	float Gravity;//重力
	float speed_x;
};
class Player
{
	std::unique_ptr<geometric_Cylinder>sphere;
    std::shared_ptr<Model> play;
    std::shared_ptr<ModelRenderer> player_render;
	XMFLOAT3 pos;
	XMFLOAT3 angle;
	XMFLOAT3 scale;
    XMFLOAT4 color;
    XMFLOAT2 speed;
	XMFLOAT3 min;
	XMFLOAT3 max;
	XMFLOAT3 before_pos;
	XMFLOAT3 now_pos;
	XMFLOAT3 cy_pos;
   // float angle;  
	float G;
	Parameter_Data parameter_data;
    //ジャンプステータス
    float Jpower; //ジャンプパワー
    float Gravity;//重力
    bool  Jflag;        //ジャンプしたかどうか
	bool exist;
    float m_jump;      //初速度
	bool floor;
	bool gravity_flag;//床のどの面に当たったか
	float color_w;
	int pattrn;
	int lispon_timer;
public:
    Player(ID3D11Device*device);
	~Player();
    void Update(int editor_modo);
	void judge(float&anim_time,float shadow);
	void judge_floor(const XMFLOAT3&obj_min, const XMFLOAT3&obj_max,float&anim_time);
	void judge_gimmick(const XMFLOAT3&obj_min, const XMFLOAT3&obj_max);
	void judge_goal(const XMFLOAT3&obj_min, const XMFLOAT3&obj_max);
    void Render(ID3D11DeviceContext* context,float elapsed_time,const DirectX::XMFLOAT4X4&WVP,const DirectX::XMFLOAT4&light_direction);
	void Render_Shadow(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection, const DirectX::XMFLOAT4&light_direction);
    void Jump();
	void Lispon();
	void Save_Parameter();
	void Load_Parameter();
	void Parameter_Editor(int editor_modo);
	const DirectX::XMFLOAT3&GetPos() { return pos; }
};

