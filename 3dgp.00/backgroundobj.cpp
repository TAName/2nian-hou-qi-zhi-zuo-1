#include "backgroundobj.h"

void BackgroundObj::initialize(std::shared_ptr<ModelResource>model_resource, std::shared_ptr<ModelRenderer> model_renderer)
{
	m_model = std::make_unique<Model>(model_resource);
	m_model_renderer = model_renderer;
	m_model->PlayAnimation(0, true);
	m_color = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
}

void BackgroundObj::Update(float elapsed_time)
{
	m_model->UpdateAnimation(1.f / 60.f);
}

void BackgroundObj::Render(float elapsed_time, ID3D11DeviceContext * devicecontext, const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4 & light)
{
	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX s, r, t;
		s = DirectX::XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);//スケール行列
		r = DirectX::XMMatrixRotationRollPitchYaw(m_angle.x, m_angle.y, m_angle.z);//回転行列
		t = DirectX::XMMatrixTranslation(m_position.x, m_position.y, m_position.z);//移動行列

		W = s*r*t;
	}

	m_model->CalculateLocalTransform();
	m_model->CalculateWorldTransform(W);
	m_model_renderer->Begin(devicecontext,
		world_view_projection,
		light	// ライトの向き
	);
	m_model_renderer->Render(devicecontext, *m_model, m_color);
	m_model_renderer->End(devicecontext);

}
