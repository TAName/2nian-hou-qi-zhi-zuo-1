#pragma once
#include"scene.h"
#include<memory>
#include <thread>
#include <mutex>
#include"sprite.h"
#include "model.h"
#include "model_renderer.h"
#include"Player.h"
#include"Board.h"
#include"sound.h"

class Scene_Game :public Scene
{
    std::unique_ptr<Player> m_player;
	std::unique_ptr<Sprite> particle;
	std::unique_ptr<Sprite>operation;
	std::unique_ptr<Sprite> Button;
	std::shared_ptr<Model>					model;
	std::shared_ptr<ModelRenderer>			model_renderer;
	std::unique_ptr<Board> board;
	int anim_num;
	DirectX::XMFLOAT4 light;
	int now_loadimg_timer;
	int now_anim;
	DirectX::XMFLOAT3 leng;
	DirectX::XMFLOAT3 borad_pos;
	float borad_scale;
	float color_w = 1.f;
	std::unique_ptr<Sound> m_select_bgm;
	std::unique_ptr<Sound> m_main_bgm;
	bool start_select_flag;
	bool start_select_time_flag;
	int start_bottun_time;
	bool sound;
public:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	Scene_Game(ID3D11Device*device);
	void Load_leng();
	void Save_leng();
	int Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
	~Scene_Game();
};
