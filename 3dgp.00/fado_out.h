#pragma once
#pragma once
#include"sprite.h"
#include<memory>
#include<DirectXMath.h>
class Fade_out
{
	std::unique_ptr<Sprite>back_out;
	bool out_flag;
	float color_w;
	Fade_out() {}
public:
	void initialize(ID3D11Device*device);
	void Update(float eelapsed_time);
	void Fado_out_Start()
	{
		out_flag = false;
		color_w = 0.f;
	}
	bool GetOutFlag() { return out_flag; }
	void Render(ID3D11DeviceContext* devicecontext);
	static Fade_out&getinctense()
	{
		static Fade_out fade_out;
		return fade_out;
	}
};
#define pFade_out (Fade_out::getinctense())