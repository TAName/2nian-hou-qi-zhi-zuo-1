#pragma once
#include"scene.h"
#include<memory>
#include <thread>
#include <mutex>
#include"sprite.h"
#include"model.h"
#include"model_renderer.h"
#include"sound.h"

class Scene_Title :public Scene
{
	std::shared_ptr<Model>					model;
	std::shared_ptr<ModelRenderer>			model_renderer;
	std::unique_ptr<Sprite> particle;
	std::unique_ptr<Sprite> title_name;
	std::unique_ptr<Sprite> select_button;
	DirectX::XMFLOAT3 position;
	std::unique_ptr<Sound> m_sound;
	std::unique_ptr<Sound> select_sound;
	int now_loading_time;
	int anim_now;
	int title_name_time;
	int title_name_anim;
	int selectbottun__time;
	bool sound;
	bool out_flag;
public:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	Scene_Title(ID3D11Device*device);
	int Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
	~Scene_Title();
};
