#pragma once
#include"sprite.h"
#include<DirectXMath.h>
#include<memory>
class Goal_Production
{
	std::unique_ptr<Sprite>goal;
	std::unique_ptr<Sprite>line;
	std::unique_ptr<Sprite>back_out;
	DirectX::XMFLOAT3 player_pos;
	bool goal_flag;
	int timer_count;
	int timer;
	float color_w;
	float font_move;
	bool particle_flag;
public:
	void initialize(ID3D11Device*device);
	void Update(float elapsed_time);
	void Render(ID3D11DeviceContext* devicecontext);
	void SetGoalFlag(bool g_flag) { goal_flag = g_flag; }
	void SetPlayer_Pos(const DirectX::XMFLOAT3&position) { player_pos = position; }
	bool TimeCounter() { return timer++%timer_count == 0; }
	bool GetGoalFlag() { return goal_flag; }
	static Goal_Production&getinctanse()
	{
		static Goal_Production goal_production;
		return goal_production;
	}
};
#define pGoal_Production (Goal_Production::getinctanse())