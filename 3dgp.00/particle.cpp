#include "particle.h"
#include"vectordelete.h"
#include"vector_Combo.h"
#include<string>
#include<time.h>

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
#include"vector_Combo.h"
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Particle::Particle(std::shared_ptr<geometric_primitive> obj, const Particle_Data & data, const DirectX::XMFLOAT3&pos)
{
	
	position.x = (static_cast<float>(rand() % 101 - 50)*0.1f*data.variation_pos.x)+ pos.x;
	position.y = (static_cast<float>(rand() % 101 - 50)*0.1f*data.variation_pos.y)+ pos.y;
	position.z = (static_cast<float>(rand() % 101 - 50)*0.1f*data.variation_pos.z)+ pos.z;
	angle = data.angle;
	scale = data.scale;
	move_angle = data.move_angle;
	accel = data.accel;
	speed.x = (static_cast<float>(rand() % 101 - 50)*0.1f*data.variation_speed.x) + data.speed.x;
	speed.y = (static_cast<float>(rand() % 101 - 50)*0.1f*data.variation_speed.y) + data.speed.y;
	speed.z = (static_cast<float>(rand() % 101 - 50)*0.1f*data.variation_speed.z) + data.speed.z;
	m_obj = obj;
	type = data.type;
	timer = data.timer_max;
	timer_split = 1.f/static_cast<float>(data.timer_max);
	color = data.color;
	start_color_w = color.w;
	//color = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	flag = true;
}


void Particle::Update(float elapsed_time)
{
	switch (type)
	{
	case 0:
		Move0();
		break;
	case 1:
		Move1(elapsed_time);
		break;
	}
	DirectX::XMMATRIX W;
	DirectX::XMMATRIX s, r, t;
	s = DirectX::XMMatrixScaling(scale.x, scale.y, -scale.z);//�X�P�[���s��
	r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//��]�s��
	t = DirectX::XMMatrixTranslation(position.x, position.y, -position.z);//�ړ��s��
	
	W = s*r*t;
	DirectX::XMStoreFloat4x4(&world, W);
}

void Particle::Move0()
{

	position.x += speed.x;
	position.y += speed.y;
	position.z += speed.z;
	speed.x += accel.x;
	speed.y += accel.y;
	speed.z += accel.z;

	color.w = static_cast<float>(timer) * timer_split*start_color_w;
	if (timer-- <= 0)flag = false;
}

void Particle::Move1(float elapsed_time)
{
	position.x += speed.x;
	position.y += speed.y;
	position.z += speed.z;
	//0.01745f
	angle.x += move_angle.x*0.01745f;
	angle.y += move_angle.y*0.01745f;
	angle.z += move_angle.z*0.01745f;
	color.w = static_cast<float>(timer) * timer_split*start_color_w;
	if (timer-- <= 0)flag = false;

}

void Particle::Render(float elapsed_time, ID3D11DeviceContext * devicecontext, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection, const DirectX::XMFLOAT4 & light)
{
	DirectX::XMFLOAT4X4 world_view_projection;
	DirectX::XMStoreFloat4x4(&world_view_projection, DirectX::XMLoadFloat4x4(&world)*DirectX::XMLoadFloat4x4(&view)*DirectX::XMLoadFloat4x4(&projection));
	//XMLoadFloat4x4
    m_obj->render(devicecontext, color, light, world_view_projection, world);

	
}
//***************************************************************************//
//  Particle_Manager
//***************************************************************************//
void Particle_Manager::initialize(ID3D11Device * device)
{

	obj = std::make_shared<geometric_Cube>(device);
	Load_Data();
	if (data.size() <= 0)
	{
		data.emplace_back();
		data.back().accel = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		data.back().angle= DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		data.back().move_angle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		data.back().scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
		data.back().speed = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		data.back().variation_pos = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		data.back().variation_speed = DirectX::XMFLOAT3(0.f,0.f,0.f);
		data.back().timer_max = 0;
		data.back().type = 0;
	}
}

void Particle_Manager::Load_Data()
{
	FILE*fp;
	int i = 0;
	for (;;)
	{
		std::string file_name = { "Data/file/particle" };
		file_name += std::to_string(i);
		file_name += ".bin";
		if (fopen_s(&fp, file_name.data(), "rb")==0)
		{
			data.emplace_back();
			i++;
			fread(&data.back(), sizeof(Particle_Data), 1,fp);
			fclose(fp);
		}
		else break;
	}
}

void Particle_Manager::Save_Data()
{
	FILE*fp;
	std::string file_name = { "Data/file/particle" };
	file_name += std::to_string(m_number);
	file_name += ".bin";
	fopen_s(&fp, file_name.data(), "wb");
	fwrite(&data.at(m_number), sizeof(Particle_Data), 1, fp);
	fclose(fp);
}

void Particle_Manager::Set_particle(int number,int num, const DirectX::XMFLOAT3&pos)
{
	if (number >= data.size())return;
	for (int i = 0; i < num; i++)
	{
		particles.emplace_back(obj, data.at(number), pos);
	}
}

void Particle_Manager::Update(float elapsed_time)
{
	for (auto&p: particles)
	{
		p.Update(elapsed_time);
	}
	Vectordelete(particles);
}

void Particle_Manager::Editor(int editor_modo)
{
	if (editor_modo != 3)return;
#ifdef USE_IMGUI
	std::vector<std::string>particle_name;
	static int create_num = 1;
	for (int i=0;i<data.size();i++)
	{
		particle_name.push_back("particle");
		particle_name.back() += std::to_string(i);
	}
	ImGui::Begin("particle_editor");
	ImGui::Combo("particle_number", &m_number, vector_getter, static_cast<void*>(&particle_name), particle_name.size());
	//data.at(type);
	ImGui::SliderFloat("scale.x", &data.at(m_number).scale.x, 0.f, 100.f);
	ImGui::SliderFloat("scale.y", &data.at(m_number).scale.y, 0.f, 100.f);
	ImGui::SliderFloat("scale.z", &data.at(m_number).scale.z, 0.f, 100.f);

	ImGui::SliderFloat("accel.x", &data.at(m_number).accel.x, -100.f, 100.f);
	ImGui::SliderFloat("accel.y", &data.at(m_number).accel.y, -100.f, 100.f);
	ImGui::SliderFloat("accel.z", &data.at(m_number).accel.z, -100.f, 100.f);

	ImGui::SliderFloat("angle.x", &data.at(m_number).angle.x, -180.f, 180.f);
	ImGui::SliderFloat("angle.y", &data.at(m_number).angle.y, -180.f, 180.f);
	ImGui::SliderFloat("angle.z", &data.at(m_number).angle.z, -180.f, 180.f);

	ImGui::SliderFloat("move_angle.x", &data.at(m_number).move_angle.x, -180.f, 180.f);
	ImGui::SliderFloat("move_angle.y", &data.at(m_number).move_angle.y, -180.f, 180.f);
	ImGui::SliderFloat("move_angle.z", &data.at(m_number).move_angle.z, -180.f, 180.f);

	ImGui::SliderFloat("speed.x", &data.at(m_number).speed.x, -10.f, 10.f);
	ImGui::SliderFloat("speed.y", &data.at(m_number).speed.y, -10.f, 10.f);
	ImGui::SliderFloat("speed.z", &data.at(m_number).speed.z, -10.f, 10.f);

	ImGui::SliderFloat("variation_pos.x", &data.at(m_number).variation_pos.x, 0.f, 50.f);
	ImGui::SliderFloat("variation_pos.y", &data.at(m_number).variation_pos.y, 0.f, 50.f);
	ImGui::SliderFloat("variation_pos.z", &data.at(m_number).variation_pos.z, 0.f, 50.f);

	ImGui::SliderFloat("variation_speed.x", &data.at(m_number).variation_speed.x, 0.f, 30.f);
	ImGui::SliderFloat("variation_speed.y", &data.at(m_number).variation_speed.y, 0.f, 30.f);
	ImGui::SliderFloat("variation_speed.z", &data.at(m_number).variation_speed.z, 0.f, 30.f);

	ImGui::SliderFloat("color.x", &data.at(m_number).color.x, 0.f, 1.f);
	ImGui::SliderFloat("color.y", &data.at(m_number).color.y, 0.f, 1.f);
	ImGui::SliderFloat("color.z", &data.at(m_number).color.z, 0.f, 1.f);
	ImGui::SliderFloat("color.w", &data.at(m_number).color.w, 0.f, 1.f);

	ImGui::SliderInt("timer_max", &data.at(m_number).timer_max, 0, 100);
	ImGui::SliderInt("create_num", &create_num, 1, 100);
	ImGui::SliderInt("type", &data.at(m_number).type, 0, 1);

	if (ImGui::Button("save"))
	{
		Save_Data();
	}
	if (ImGui::Button("new_particle"))
	{
		data.emplace_back();
	}
	if (ImGui::Button("create"))
	{
		Set_particle(m_number, create_num, DirectX::XMFLOAT3(0.f, 0.f, 0.f));
	}
	ImGui::End();
#endif
}

void Particle_Manager::Render(float elapsed_time, ID3D11DeviceContext * devicecontext, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection, const DirectX::XMFLOAT4 & light)
{
	for (auto&p : particles)
	{
		p.Render(elapsed_time, devicecontext, view, projection,light);
	}
}
