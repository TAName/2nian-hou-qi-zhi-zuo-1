#pragma once
#include"scene.h"
#include<memory>
#include <thread>
#include <mutex>
#include"sprite.h"
#include "model.h"
#include "model_renderer.h"
#include"Ranking.h"
#include"sound.h"

class Scene_Result :public Scene
{
	std::shared_ptr<Model>					model;
	std::shared_ptr<ModelRenderer>			model_render;
	std::unique_ptr<Sprite> particle;
	int select_modo;
	std::unique_ptr<Sound>select_sound;
	std::unique_ptr<Sound>m_bgm;
public:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	Scene_Result(ID3D11Device*device);
	int Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
	~Scene_Result();
};