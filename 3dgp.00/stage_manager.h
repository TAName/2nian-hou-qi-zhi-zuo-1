#pragma once
#include"stageobj.h"
#include"backgroundobj.h"
#include<vector>
#include"sprite.h"
#include"sound.h"
struct Save_Stage_Data
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 angle;
	DirectX::XMFLOAT3 scale;
	int color_type;
	int obj_type;
};
struct Save_Background_Data
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 angle;
	DirectX::XMFLOAT3 scale;
	float color_w;
	int type;
};
#define BACK_OBJ 6
class Stage_Manager
{
	DirectX::XMFLOAT4 color[2];
	int color_type;
	std::shared_ptr<Model>stage[3];
	std::shared_ptr<ModelResource>backgraund[BACK_OBJ];
	std::shared_ptr<ModelRenderer>stage_renderer;
	std::shared_ptr<ModelRenderer>editor_renderer;
	std::vector<StageObj>s_obj;
	std::vector<BackgroundObj>b_obj;
	std::unique_ptr<Sprite>select_sprite;
	std::unique_ptr<Sprite>select_back;
	std::unique_ptr<Sprite>select_font;
	std::unique_ptr<Sprite>select_star;
	std::unique_ptr<Sprite>select_operation;
	std::unique_ptr<Sprite>select_tutorial;
	std::unique_ptr<Sound>decision;
	bool create;
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 angle;
	DirectX::XMFLOAT3 scale;
	bool stage_select_flag;
	int now_Stage_No;
	int Stage_Max;
	int stage_obj_type;
	int change_stage;
	Stage_Manager() { Set_Stage_Max(); }
	void Set_Stage_Max();
	int color_obj_modo;
	ID3D11Device*de;
	int select_modo;
	float select_color_w;
public:
	static const int stage_column = 5;
	void initialize(ID3D11Device*device);
	void Load_Stage(int stage_num);//ステージデータの読み込み
	void Save_Stage();//ステージデータの保存
	void Updata(float elapsed_time, int editor_modo);
	void StageObjColorModo();
	bool Stage_Select(float eelapsed_time);
	void Select_Move();
	void Obj_Editor();
	void Move_Set_Pos();
	void Stage_Obj_Editor();
	void BackGround_Obj_Editor();
	void Relese()
	{
		b_obj.clear();
		s_obj.clear();
	}
	//セッター
	void Set_Floor();
	void Set_Gimmick();
	void Set_Goal();
	void SetChangeStage(int change) 
	{ 
		change_stage = change; 
	}
	void Set_Change_Modo(int color_modo);
	void Set_Stage(int number)
	{
		now_Stage_No = number;
		Load_Stage(number);
	}
	void SetStage_Select(bool select) { stage_select_flag = select; }
	//ゲッター
	int GetNowStageNo() { return now_Stage_No; }
	int GetChangeStage() { return change_stage; }
	bool GetStage_Select() { return stage_select_flag; }
	std::vector<StageObj>&GetStageObjs()
	{
		return s_obj;
	}
	~Stage_Manager() {  }
	void Stage_Select_Render(ID3D11DeviceContext * devicecontext);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext, const DirectX::XMFLOAT4X4&world_view_projection, const DirectX::XMFLOAT4&light);
	static Stage_Manager&getinctance()
	{
		static Stage_Manager s_manager;
		return s_manager;
	}

};
#define pStage_Manager (Stage_Manager::getinctance())