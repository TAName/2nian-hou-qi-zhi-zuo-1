#include "scene_over.h"
#include "UI.h"
#include "scene_manager.h"
#include"keyboad.h"
#include "Ranking.h"
#include "stage_manager.h"
#include"fado_out.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
Scene_Over::Scene_Over(ID3D11Device * device)
{
    //ゲームオーバー
	select_sound = std::make_unique<Sound>("Data/sound/e.wav");//決定音
	m_bgm = std::make_unique<Sound>("Data/sound/Future_Bridge.wav");//bgm
    GameOver = std::make_unique<Sprite>(device, L"Data/image/GAME_OVER.png");
    black = std::make_unique<Sprite>(device, L"Data/image/siro.png");
    Back_Sprite = std::make_unique<Sprite>(device,L"Data/image/GAMEOVER_BACK.png");
    arrow= std::make_unique<Sprite>(device, L"Data/image/arrow.png");
    alpha = 0.0f;
    arrow_x = 1610;
    arrow_y = 1010;
	select_modo = 0;
	pFade_out.initialize(device);
	m_bgm->Play(true);
	m_bgm->SetVolume(1.0);
}

int Scene_Over::Update(float elapsed_time)
{
    if (arrow_x == 1610 && pKeyBoad.RisingState(KeyLabel::SPACE))
    {
		select_modo = 1;
		pFade_out.Fado_out_Start();
		select_sound->Play();
		select_sound->SetVolume(1.0);
    }
    if (arrow_x == 1090 && pKeyBoad.RisingState(KeyLabel::SPACE))
    {

        pStage_Manager.SetStage_Select(false);
		pFade_out.Fado_out_Start();
		select_sound->Play();
		select_sound->SetVolume(1.0);
		select_modo = 2;
    }
	if (select_modo > 0)
	{
		pFade_out.Update(elapsed_time);
		if (pFade_out.GetOutFlag())return select_modo;
	}
	else
	{
		Black(elapsed_time);
	}
    return 0;

    /*   if (pKeyBoad.RisingState(KeyLabel::SPACE))
    {
    blackF = true;
    }
    if (blackF)
    {
    Black(elapsed_time);
    }
    if (alpha >= 1.f)
    {
    blackF = false;
    return 1;
    }*/
}

void Scene_Over::Render(float elapsed_time, ID3D11DeviceContext * devicecontext)
{

    
  
    Back_Sprite->render(devicecontext, 0.0f, 0.0f, 1920, 1080, 0, 0, 1920, 1080, 1, 1, 1, 1, 1);
    GameOver->render(devicecontext, 530.0f, 410.0f, 820.0f,150.0f, 0, 0, 768, 96, 0, 1, 1, 1, 1);
    arrow->render(devicecontext, static_cast<float>(arrow_x), static_cast<float>(arrow_y), 50.f, 50.f, 0.f, 0.f, 96.f, 96.f, 0.f, 1.f, 1.f, 1.f, 1.f);
   // black->render(devicecontext, 0.f, 0.f, 1980.f, 1080.f, 0.f, 0.f, 1024.f, 1024.f, 0.f, 0.f, 0.f, 0.f, alpha);
	pFade_out.Render(devicecontext);
}

Scene_Over::~Scene_Over()
{
	if(m_bgm)m_bgm->Pause();
}


void Scene_Over::Black(float elapsedTime)
{
  if (pKeyBoad.RisingState(KeyLabel::RIGHT))
    {
        arrow_x = 1610;
        arrow_y = 1010;

    }

    if (pKeyBoad.RisingState(KeyLabel::LEFT))
    {

        arrow_x = 1090;
        arrow_y = 1010;

    }
}
