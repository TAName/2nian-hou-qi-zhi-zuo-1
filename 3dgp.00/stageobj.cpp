#include "stageobj.h"

void StageObj::initialize(std::shared_ptr<Model> model, std::shared_ptr<ModelRenderer> model_renderer)
{
	m_model = model;
	m_model_renderer = model_renderer;
	m_color = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	m_scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	angle = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	position = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	flag = true;
	m_modo = 0;
}

void StageObj::Update(float elapsed_time)
{
	switch (m_obj_type)
	{
	case 0:
		break;
	case 1:
		break;
	}
}

void StageObj::Render(float elapsed_time, ID3D11DeviceContext * devicecontext, const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4 & light)
{
	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX s, r, t;
		s = DirectX::XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);//スケール行列
		r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
		t = DirectX::XMMatrixTranslation(position.x, position.y, position.z);//移動行列

		W = s*r*t;
	}

	m_model->CalculateLocalTransform();
	m_model->CalculateWorldTransform(W);
	m_model_renderer->Begin(devicecontext,
		world_view_projection,
		light	// ライトの向き
	);
	m_model_renderer->Render(devicecontext, *m_model, m_color);
	m_model_renderer->End(devicecontext);

}
