#include "scene_game.h"
#include "fbx_load.h"
#include"camera.h"
#include"stage_manager.h"
#include"particle.h"
#include"goal_production.h"
#include"UI.h"
#include"keyboad.h"
#include"fado_out.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
//*****************************************************************//
//  初期化
//*****************************************************************//
Scene_Game::Scene_Game(ID3D11Device * device)
{
	anim_num = 0;
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device *device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);
        m_player = std::make_unique<Player>(device);
		pStage_Manager.initialize(device);
		pParticle_Manager.initialize(device);
		board = std::make_unique<Board>(device, L"Data/image/かめれおんの拝啓.png");
	}, device);
	light = DirectX::XMFLOAT4(0.f, -1, 0.5f, 0);
	//432,100
	m_select_bgm = std::make_unique<Sound>("Data/sound/game_BGM.wav");
	m_main_bgm = std::make_unique<Sound>("Data/sound/NEO_UNIVERSE.wav");
	start_select_flag = false;
	start_select_time_flag = false;
	particle = std::make_unique<Sprite>(device, L"Data/image/NOW_LOADING.png");
	Button = std::make_unique<Sprite>(device, L"Data/image/push_space.png");
	operation = std::make_unique<Sprite>(device, L"Data/image/controls.png");
	pCamera.Create(DirectX::XMFLOAT3(10.0f, 50.0f, -500.0f), DirectX::XMFLOAT3(0.0f, 50.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f));
	now_loadimg_timer = 0;
	now_anim = 0;
	leng = DirectX::XMFLOAT3(-600, 100, -600);
	borad_pos=DirectX::XMFLOAT3(100, -300, -400);
	borad_scale=1000;
	Load_leng();
	pStage_Manager.SetChangeStage(0);
	pGoal_Production.initialize(device);
	pFade_out.initialize(device);
	pUI.Initialize(device);
	color_w = 1.f;
	start_bottun_time = 0;
}
//データの読み込み
void Scene_Game::Load_leng()
{
	FILE*fp;
	if (fopen_s(&fp, "Data/file/camera_leng.bin", "rb")==0)
	{
		fread(&leng, sizeof(DirectX::XMFLOAT3), 1, fp);
		fclose(fp);
	}
	if (fopen_s(&fp, "Data/file/board.bin", "rb") == 0)
	{
		fread(&borad_pos, sizeof(DirectX::XMFLOAT3), 1, fp);
		fread(&borad_scale, sizeof(float), 1, fp);
		fclose(fp);

	}
}
//セーブ
void Scene_Game::Save_leng()
{
	FILE*fp;
	fopen_s(&fp, "Data/file/camera_leng.bin", "wb");
	fwrite(&leng, sizeof(DirectX::XMFLOAT3), 1, fp);
	fclose(fp);
	fopen_s(&fp, "Data/file/board.bin", "wb");
	fwrite(&borad_pos, sizeof(DirectX::XMFLOAT3), 1, fp);
	fwrite(&borad_scale, sizeof(float), 1, fp);
	fclose(fp);
	sound = false;
}
//*****************************************************************//
//  更新
//*****************************************************************//
int Scene_Game::Update(float elapsed_time)
{
	if (IsNowLoading())
	{
		return 0;
	}
	EndLoading();
	if (!start_select_flag)
	{
		if (pKeyBoad.RisingState(KeyLabel::SPACE))
		{
			start_select_flag = true;
		}
		return 0;
	}
	//ステージセレクト
	if (!pStage_Manager.Stage_Select(elapsed_time))
	{
		if (!m_select_bgm->Playing())
		{
			m_select_bgm->Play(true);
			m_select_bgm->SetVolume(1.0f);
		}
		m_select_bgm->Update();

		return 0;
	}
	if (!sound)
	{
		m_select_bgm->Stop();
		m_main_bgm->Play(true);
		m_main_bgm->SetVolume(1.0);
		sound = true;
	}
	m_main_bgm->Update();
	//エディター
	static int p = 2;
#ifdef USE_IMGUI

	ImGui::Begin("game");

	ImGui::RadioButton("play", &p, 0);
	ImGui::RadioButton("stage_editor", &p, 1);
	ImGui::RadioButton("player_editor", &p, 2);
	ImGui::RadioButton("particle_editor", &p, 3);
	ImGui::SliderFloat("x", &leng.x, -1000.f, 1000.f);
	ImGui::SliderFloat("y", &leng.y, -1000.f, 1000.f);
	ImGui::SliderFloat("z", &leng.z, -1000.f, 1000.f);
	ImGui::SliderFloat("borad_posx", &borad_pos.x, -10000.f, 10000.f);
	ImGui::SliderFloat("borad_posy", &borad_pos.y, -10000.f, 10000.f);
	ImGui::SliderFloat("borad_posz", &borad_pos.z, -10000.f, 10000.f);
	ImGui::SliderFloat("borad_scale", &borad_scale, 0.f, 30000.f);
	ImGui::SliderFloat("color_w", &color_w, 0.f, 1.f);

	if (ImGui::Button("save"))
	{
		Save_leng();
	}
	ImGui::End();
#else
	p = 0;
#endif
	//各処理の更新
	if(!pGoal_Production.GetGoalFlag())m_player->Update(p);
	else
	{
		pGoal_Production.Update(elapsed_time);
		if(pGoal_Production.TimeCounter())m_player->Update(p);
	}
	pCamera.SetCamera(m_player->GetPos(), leng, DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), pCamera.Camera_TYPE::RELATIVE_POS);
	pStage_Manager.Updata(elapsed_time,p);
	m_player->Parameter_Editor(p);
	pCamera.Updata();
	pParticle_Manager.Update(elapsed_time);
	pParticle_Manager.Editor(p);
	pUI.Update(elapsed_time,p);
	if (pUI.time <= 0)
	{

		return 4;
	}

	return pStage_Manager.GetChangeStage();
  
}
//*****************************************************************//
//  描画
//*****************************************************************//
void Scene_Game::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
	if (!start_select_time_flag)
	{
		operation->render(devicecontext, 0, 0, 1920, 1080, 0, 0, 1920, 1080, 0, 1, 1, 1, 1);
		if (IsNowLoading())
		{

			particle->render(devicecontext, 1300, 900, 320.f + (32.f*static_cast<float>(now_anim)), 32, 0, 0, 640.f + (64.f*static_cast<float>(now_anim)), 64, 0, 1, 1, 1, 1);
			if (now_loadimg_timer++ % 20 == 0)
			{
				now_anim++;
				if (now_anim == 4)now_anim = 0;
			}
			return;
		}

		if (!start_select_flag)
		{
			if (start_bottun_time++ % 35 >= 14)Button->render(devicecontext, 1300, 900, 480, 48, 0, 0, 960.f, 96.f, 0, 1, 1, 1, 1);
		}
		else start_select_time_flag = true;
		return;
	}
	if (!pStage_Manager.GetStage_Select())
	{
		pStage_Manager.Stage_Select_Render(devicecontext);
		return;
	}

	const float PI = 3.14f;
	//ワールド行列を作成
	DirectX::XMMATRIX W;
	{
		static float rz = 0;
		//rz += 0.2f*(PI / 180.f);
		static float rt = (PI / 180.f)*45.f;
		//rt += 0.2f*(PI / 180.f);
		//Z軸方向に前後移動してみる
		static float mz = 0;
		//mz += 0.002f;
		float tz = sinf(mz)*5.0f;

		DirectX::XMFLOAT3 scale(1, 1, 1);
		DirectX::XMFLOAT3 rotate(0, 0, rz);
		DirectX::XMFLOAT3 translate(0, 0, 0);

		DirectX::XMMATRIX s, r, t;
		s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
		r = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);//回転行列
		t = DirectX::XMMatrixTranslation(translate.x, translate.y, translate.z);//移動行列

		W = s*r*t;
	}
	//ビュー行列
	DirectX::XMMATRIX V;
	{
		//カメラの設定
		DirectX::XMVECTOR eye, focus, up;
		eye = DirectX::XMVectorSet(pCamera.GetEye().x, pCamera.GetEye().y, pCamera.GetEye().z, 1.0f);//視点
		focus = DirectX::XMVectorSet(pCamera.GetFocus().x, pCamera.GetFocus().y, pCamera.GetFocus().z, 1.0f);//注視点
		up = DirectX::XMVectorSet(pCamera.GetUp().x, pCamera.GetUp().y, pCamera.GetUp().z, .0f);//上ベクトル

		V = DirectX::XMMatrixLookAtLH(eye, focus, up);
	}
	DirectX::XMMATRIX P;
	{
		//画面サイズ取得のためビューポートを取得
		D3D11_VIEWPORT viewport;
		UINT num_viewports = 1;
		devicecontext->RSGetViewports(&num_viewports, &viewport);

		//角度をラジアン(θ)に変換
		float fov_y = 30 * (PI / 180.f);//角度
		float aspect = viewport.Width / viewport.Height;//画面比率
		float near_z = 0.1f;//表示最近面までの距離
		float far_z = 100000.0f;//表紙最遠面までの距離

		P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
	}
	//ビュープロジェクション行列
	DirectX::XMFLOAT4X4 world_view_projection;
	{
		DirectX::XMMATRIX WVP;
		DirectX::XMMATRIX C = DirectX::XMMatrixSet(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, -1, 0,
			0, 0, 0, 1
		);

		WVP = C*V*P;
		DirectX::XMStoreFloat4x4(&world_view_projection, WVP);
	}
	//ビュー行列、プロジェクション行列を変換
	DirectX::XMFLOAT4X4 view, projiution;
	DirectX::XMStoreFloat4x4(&view, V);
	DirectX::XMStoreFloat4x4(&projiution, P);
	//背景
	DirectX::XMFLOAT3 b_pos = DirectX::XMFLOAT3(borad_pos.x, borad_pos.y, borad_pos.z+pCamera.GetFocus().z);
	board->render(devicecontext, b_pos, borad_scale, view, projiution, DirectX::XMFLOAT4(1.f, 1.f, 1.f, color_w));
	//オブジェクト
    m_player->Render(devicecontext, elapsed_time, world_view_projection, light);
	m_player->Render_Shadow(devicecontext, view, projiution, light);
	pStage_Manager.Render(elapsed_time, devicecontext, world_view_projection, light);
	//パーティクル
	pParticle_Manager.Render(elapsed_time, devicecontext, view, projiution, light);

	pUI.Render(devicecontext);

    pGoal_Production.Render(devicecontext);

}

Scene_Game::~Scene_Game()
{
	pCamera.Destroy();
	pStage_Manager.Relese();
	pParticle_Manager.Relese();
	if (m_select_bgm)
		m_select_bgm->Pause();
	if (m_main_bgm)
		m_main_bgm->Pause();
	EndLoading();

}
