#include "scene_title.h"
#include "fbx_load.h"
#include"camera.h"
#include "UI.h" //消す
#include"particle.h"
#include"keyboad.h"
#include"fado_out.h"

Scene_Title::Scene_Title(ID3D11Device * device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device *device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);

	//skinned = std::make_unique<skinned_mesh>(device, "Data/FBX/danbo_fbx/danbo_taiki.fbx", false);
		const char* fbx_filename = "Data/FBX/new_player_anim.fbx";
		std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		FbxLoader fbx_loader;
		fbx_loader.Load(fbx_filename, *model_data);

		std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
		model = std::make_shared<Model>(model_resource);
		model_renderer = std::make_shared<ModelRenderer>(device);
		pParticle_Manager.initialize(device);

		model->PlayAnimation(0, true);

	}, device);
	m_sound = std::make_unique<Sound>("Data/sound/Power.wav");
	select_sound = std::make_unique<Sound>("Data/sound/e.wav");
	particle = std::make_unique<Sprite>(device, L"Data/image/NOW_LOADING.png");
	title_name = std::make_unique<Sprite>(device, L"Data/image/CHANGE_COLOR_ani.png");
	select_button = std::make_unique<Sprite>(device, L"Data/image/push_space.png");
	position = DirectX::XMFLOAT3(0, 0, 0);
	pCamera.Create(DirectX::XMFLOAT3(10.0f, 50.0f, -500.0f), DirectX::XMFLOAT3(0.0f, 50.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f));
	now_loading_time = 0;
	anim_now = 0;
	title_name_anim = 0;
	title_name_time = 0;
	selectbottun__time = 0;
	sound = false;
	pFade_out.initialize(device);
	out_flag = false;
}

int Scene_Title::Update(float elapsed_time)
{

	if (IsNowLoading())
	{
		if (now_loading_time++ % 20 == 0)
		{
			anim_now++;
			if (anim_now == 4)anim_now = 0;
		}
		return 0;
	}
	EndLoading();
	if (!sound)
	{
		m_sound->Play(true);
		m_sound->SetVolume(1.0f);
		sound = true;
	}
	//m_sound->Update();

	if (pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		m_sound->Stop();
		select_sound->Play();
		select_sound->SetVolume(1.0);
		pFade_out.Fado_out_Start();
		out_flag = true;
	}
	pFade_out.Update(elapsed_time);
	if (pFade_out.GetOutFlag()&&out_flag)return 2;
	model->UpdateAnimation(2.0f / 60.0f);
	//position.z += 10.f;
	pParticle_Manager.Set_particle(5, 5, position);
	pCamera.SetCamera(position, DirectX::XMFLOAT3(-500,150,500), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), pCamera.Camera_TYPE::RELATIVE_POS);
	pCamera.Updata();
	pParticle_Manager.Update(elapsed_time);
	if (title_name_time++ % 20 == 0)
	{
		if (title_name_anim++ == 12)
		{
			title_name_anim = 0;
		}
	}
	//pParticle_Manager.Editor(3);
	return 0;

}

void Scene_Title::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
	if (IsNowLoading())
	{
		particle->render(devicecontext, 1300, 900, 320.f + (32.f*static_cast<float>(anim_now)), 32, 0, 0, 640.f + (64.f*static_cast<float>(anim_now)), 64, 0, 1, 1, 1, 1);
		return;
	}
	title_name->render(devicecontext, 200, 200, 768.f , 64, 768.f *static_cast<float>(title_name_anim), 0, 768.f, 64, 0, 1, 1, 1, 1);
	if(selectbottun__time++%35>=14)select_button->render(devicecontext, 700, 800, 480, 48, 0, 0, 960.f, 96.f, 0, 1, 1, 1, 1);

	const float PI = 3.14f;
	//ワールド行列を作成
	DirectX::XMMATRIX W;
	{
		static float rz = 0;
		//rz += 0.2f*(PI / 180.f);
		static float rt = (PI / 180.f)*45.f;
		//rt += 0.2f*(PI / 180.f);
		//Z軸方向に前後移動してみる
		static float mz = 0;
        static float my = 0;
		//mz += 0.002f;
		float tz = sinf(mz)*5.0f;
        float ty = sinf(my)*4.0f;
		DirectX::XMFLOAT3 scale(8,8,8);
		DirectX::XMFLOAT3 rotate(0,0,0);
		DirectX::XMMATRIX s, r, t;
		s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
		r = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);//回転行列
		t = DirectX::XMMatrixTranslation(position.x, position.y, position.z);//移動行列

		W = s*r*t;


       
	}
	//ビュー行列
	DirectX::XMMATRIX V;
	{
		//カメラの設定
		DirectX::XMVECTOR eye, focus, up;
		eye = DirectX::XMVectorSet(pCamera.GetEye().x, pCamera.GetEye().y, pCamera.GetEye().z, 1.0f);//視点
		focus = DirectX::XMVectorSet(pCamera.GetFocus().x, pCamera.GetFocus().y, pCamera.GetFocus().z, 1.0f);//注視点
		up = DirectX::XMVectorSet(pCamera.GetUp().x, pCamera.GetUp().y, pCamera.GetUp().z, 1.0f);//上ベクトル

		V = DirectX::XMMatrixLookAtLH(eye, focus, up);
	}
	DirectX::XMMATRIX P;
	{
		//画面サイズ取得のためビューポートを取得
		D3D11_VIEWPORT viewport;
		UINT num_viewports = 1;
		devicecontext->RSGetViewports(&num_viewports, &viewport);

		//角度をラジアン(θ)に変換
		float fov_y = 30 * (PI / 180.f);//角度
		float aspect = viewport.Width / viewport.Height;//画面比率
		float near_z = 0.1f;//表示最近面までの距離
		float far_z = 1000.0f;//表紙最遠面までの距離

		P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
	}

	
	//立方体の描画
	DirectX::XMFLOAT4X4 world_view_projection;
	{
		DirectX::XMMATRIX WVP;
		DirectX::XMMATRIX C = DirectX::XMMatrixSet(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, -1, 0,
			0, 0, 0, 1
		);

		WVP = C*V*P;
		DirectX::XMStoreFloat4x4(&world_view_projection, WVP);
	}
	DirectX::XMFLOAT4X4 view, projection;
	DirectX::XMStoreFloat4x4(&view, V);
	DirectX::XMStoreFloat4x4(&projection, P);

	//立方体の描画
	model->CalculateLocalTransform();
	model->CalculateWorldTransform(W);
	model_renderer->Begin(devicecontext,
		world_view_projection,
		DirectX::XMFLOAT4(1, -1, -1, 0)	// ライトの向き
	);
	model_renderer->Render(devicecontext, *model,DirectX::XMFLOAT4(1.f,1.f,1.f,1.0f));
	model_renderer->End(devicecontext);
	pParticle_Manager.Render(elapsed_time, devicecontext, view, projection, DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.0f));
	pFade_out.Render(devicecontext);
}

Scene_Title::~Scene_Title()
{
	pCamera.Destroy();
	pParticle_Manager.Relese();
	//m_sound->Stop();
	EndLoading();

}
