#include "UI.h"
#include "stage_manager.h"
#include <windows.h>
#include"goal_production.h"
#include "Ranking.h"
#include"keyboad.h"
UI::UI()
{
    FILE*fp;
    int i = 0;
    for (;;)
    {
        int rank[5];
        std::string FileAD = { "Data/file/Ranking" };
        FileAD += std::to_string(i);
        FileAD += ".txt";

        if (fopen_s(&fp, FileAD.data(), "rb") == 0)
        {
            fscanf_s(fp, "%d,%d,%d,%d,%d", &rank[0], &rank[1], &rank[2], &rank[3], &rank[4]);
            BestScore.push_back(rank[4]);
            i++;
            fclose(fp);
        }
        else break;
    }
}
void UI::Initialize(ID3D11Device * device)
{

    time = 100;
    time1 = 100;
    t_Sprite = std::make_unique<Sprite>(device, L"Data/image/num_font.png");
    Stagetime_Sprite = std::make_unique<Sprite>(device, L"Data/image/stage_font.png");
    Clock = std::make_unique<Sprite>(device, L"Data/image/time.png");
    Best_Sprite = std::make_unique<Sprite>(device, L"Data/image/best_time.png");
}

void UI::t_rigit()
{
    ////桁調べる　
    //for (int i = 0; i < 5; i++)
    //{
    //   rigit_t[i] = timer_r[i];

    //    while (rigit_t[i] != 0)
    //    {
    //        rigit_t[i] = rigit_t[i] / 10;
    //        ++digit_[i];
    //    }

    //}


    //桁をバラバラにする
    for (int i = 0; i < 4; i++)
    {
        t_digit[i] = (timer_r % 10);
        timer_r /= 10;
    }
}


void UI::Update(float elapsedtime, int p)
{
    if (p != 0)
    {
        time = 10000;
        time1 = 100;
        timer_r = time;
        t_rigit();
        return;
    }
    if (pGoal_Production.GetGoalFlag())return;
    time1 -= elapsedtime;
    time = static_cast<int>(time1 * 100);
    //タイム画像にする
    timer_r = time;
    t_rigit();
    if (time <= 0)
    {
        pStage_Manager.SetChangeStage(4);
    }
}

void UI::Render(ID3D11DeviceContext* immediate_context)
{
    float x = 450.f;
    float y = 20.f;

    for (int i = 0; i < 4; i++)
    {
        t_Sprite->render(immediate_context, x - static_cast<float>(96 * i), y, 96.f, 96.f, static_cast<float>(96 * t_digit[i]), 0.f, 96.f, 96.f, 0.f, 1.f, 1.f, 1.f, 1.f);
    }
    //:表示
    t_Sprite->render(immediate_context, 310, 20, 96, 96, 96 * 10, 0, 96, 96, 0, 1, 1, 1, 1);
    //時計
    Clock->render(immediate_context, 70, 20, 96, 96, 0, 0, 96, 96, 0, 1, 1, 1, 1);
}

void UI::BestScoreRender(ID3D11DeviceContext* immediate_context, float pls_x, float pos_y, float size_x, float size_y, int number)
{

    if (number >= BestScore.size())return;
    score = BestScore.at(number);
    //桁をバラバラにする
    float pos_x = pls_x + (size_x*0.75f);
    float position_y = pos_y + (size_y*0.56f);
    for (int i = 0; i < 4; i++)
    {

        score_render[i] = (score % 10);
        score /= 10;
        Stagetime_Sprite->render(immediate_context, pos_x, position_y, size_x*0.15f, size_y*0.15f, static_cast<float>(96 * score_render[i]), 0.f, 96.f, 96.f, 0.f, 1.f, 1.f, 1.f, 1.f);
        pos_x -= size_x*0.25f;

    }

    //:表示
    float x = pls_x + (size_x*0.4f);
    float y = pos_y + (size_y*0.56f);
    for (int i = 0; i < 1; i++)
    {
        Stagetime_Sprite->render(immediate_context, x, y, size_x*0.15f, size_y*0.15f, 96.f * 10.f, 0.f, 96.f, 96.f, 0.f, 1.f, 1.f, 1.f, 1.f);
        x -= size_x*0.25f;
    }

}
//BestTime表示
void UI::BestTimeRender(ID3D11DeviceContext* immediate_context, float pls_x, float pos_y, float size_x, float size_y, int number)
{
    float pos_x = pls_x + (size_x*0.01f);
    float position_y = pos_y + (size_y*0.45f);
    for (int i = 0; i < 4; i++)
    {
        Best_Sprite->render(immediate_context, pos_x, position_y, size_x*0.6f, size_y*0.1f, 768, 0, 768, 96, 0, 1, 1, 1, 1);
    }
    pos_x -= size_x*0.25f;

}

UI::~UI()
{
    BestScore.clear();
}

void UI::PlayerTimeRender(ID3D11DeviceContext* immediate_context)
{
    //playerTime

    float x = 343.f;
    float y = 418.f;

    for (int i = 0; i < 4; i++)
    {
        t_Sprite->render(immediate_context, x - static_cast<float>(96 * i), y, 96.f, 96.f, static_cast<float>(96 * t_digit[i]), 0.f, 96.f, 96.f, 0.f, 1.f, 1.f, 1.f, 1.f);
    }
}
