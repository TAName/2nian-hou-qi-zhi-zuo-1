#pragma once
#include "model.h"
#include "model_renderer.h"
#include<memory>

class StageObj
{
	std::shared_ptr<Model>m_model;
	std::shared_ptr<ModelRenderer>m_model_renderer;
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 m_scale;
	DirectX::XMFLOAT3 angle;
	DirectX::XMFLOAT4 m_color;
	DirectX::XMFLOAT3 min;
	DirectX::XMFLOAT3 max;
	bool flag;
	int m_color_type;
	int m_modo;
	int m_obj_type;
public:
	void initialize(std::shared_ptr<Model>model, std::shared_ptr<ModelRenderer>model_renderer);
	operator const bool() const { return flag; };

	//セッタ―
	void SetScale(const DirectX::XMFLOAT3&scale) 
	{ 
		m_scale = scale;
		min = DirectX::XMFLOAT3(position.x - m_scale.x, position.y - m_scale.y, position.z - m_scale.z);
		max = DirectX::XMFLOAT3(position.x + m_scale.x, position.y + m_scale.y, position.z + m_scale.z);
	}
	void SetScale2(const DirectX::XMFLOAT3&scale)
	{
		m_scale = scale;
		min = DirectX::XMFLOAT3(position.x - m_scale.x, position.y, position.z - m_scale.z);
		max = DirectX::XMFLOAT3(position.x + m_scale.x, position.y + (m_scale.y*2.f), position.z + m_scale.z);
	}
	void SetPosition(const DirectX::XMFLOAT3&pos) { position = pos; }
	void SetAngle(const DirectX::XMFLOAT3&rotate) { angle = rotate; }
	void SetColor(const DirectX::XMFLOAT4&color) { m_color = color; }
	void SetColorType(const int color_type) 
	{ 
		m_color_type = color_type;
		m_modo = color_type;
	}
	void SetFlag(bool flag) { this->flag = flag; }
	void SetModo(int modo) { m_modo = modo; }
	void SetObjType(const int obj_type) { m_obj_type = obj_type; }
	//ゲッター
	DirectX::XMFLOAT3&GetPosition() { return position; }
	DirectX::XMFLOAT3&GetScale() { return m_scale; }
	DirectX::XMFLOAT3&Getangle() { return angle; }
	int&GetColorType() { return m_color_type; }
	DirectX::XMFLOAT3&GetMin() { return min; }
	DirectX::XMFLOAT3&GetMax() { return max; }
	int&GetModo() { return m_modo; }
	int&GetObjType() { return m_obj_type; }
	void Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext, const DirectX::XMFLOAT4X4&world_view_projection, const DirectX::XMFLOAT4&light);
};