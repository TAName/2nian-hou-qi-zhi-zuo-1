#include"BlendState.h"
#include	<assert.h>

#if 0
HRESULT Blend::SetBlend(ID3D11Device *Device, int state)
{
	HRESULT hr = S_OK;
	D3D11_BLEND_DESC blDesc;
	switch (state)
	{
	case BlendModo::NOBLEND:
		ZeroMemory(&blDesc, sizeof(blDesc));
		blDesc.RenderTarget[0].BlendEnable = true;
		//画像の色
		blDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;//画像同士の色の計算
															//アルファ値(固定?)
		blDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;//COLOR(1,1,1,1)   :今回描画されるもの
		blDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;//COLOR(0,0,0,0) :既に描画されてるもの
		blDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;//アルファ値の計算

		blDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		//virtual HRESULT STDMETHODCALLTYPE CreateBlendState(
		// /* [annotation] */
		// _In_  const D3D11_BLEND_DESC *pBlendStateDesc,
		// /* [annotation] */
		// _Out_opt_  ID3D11BlendState **ppBlendState) = 0;

		hr = Device->CreateBlendState(&blDesc, &blend);

		break;
	case BlendModo::ADD:
		break;
	case BlendModo::SUB:
		break;
	case BlendModo::MUL:
		break;
	}
	return hr;
}
#else


ID3D11BlendState*		blender::blend_state[MODE_MAX] = { 0 };
bool					blender::bLoad = false;
blender::BLEND_MODE		blender::nowMode;
ID3D11DeviceContext*	blender::keepContext;


struct _blend_data
{
	D3D11_BLEND		SrcBlend;
	D3D11_BLEND		DestBlend;
	D3D11_BLEND_OP	BlendOp;
	D3D11_BLEND		SrcBlendAlpha;
	D3D11_BLEND		DestBlendAlpha;
	D3D11_BLEND_OP	BlendOpAlpha;

}
blend_data[blender::MODE_MAX] =
{
	{	//	NONE
		D3D11_BLEND_ONE,				//	SrcBlend
		D3D11_BLEND_ZERO,				//	DestBlend
		D3D11_BLEND_OP_ADD,				//	BlendOp
		D3D11_BLEND_ONE,				//	SrcBlendAlpha
		D3D11_BLEND_ZERO,				//	DestBlendAlpha
		D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	NONE
	{	//	ALPHA
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_INV_SRC_ALPHA,		//	DestBlend
		D3D11_BLEND_OP_ADD,				//	BlendOp
		D3D11_BLEND_ONE,				//	SrcBlendAlpha
		D3D11_BLEND_ZERO,				//	DestBlendAlpha
		D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	ALPHA
	{	//	ADD
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_ONE,				//	DestBlend
		D3D11_BLEND_OP_ADD,				//	BlendOp
		D3D11_BLEND_ONE,				//	SrcBlendAlpha
		D3D11_BLEND_ZERO,				//	DestBlendAlpha
		D3D11_BLEND_OP_ADD,				//	BlendOpAlpha
	},	//	ADD
	{	//	SUB
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_ONE,				//	DestBlend
		D3D11_BLEND_OP_REV_SUBTRACT,	//	BlendOp
		D3D11_BLEND_ONE,				//	SrcBlendAlpha
		D3D11_BLEND_ZERO,				//	DestBlendAlpha
		D3D11_BLEND_OP_ADD,				//	BlendOpAlpha
	},	//	SUB
	//{	//	REPLACE
	//	D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
	//	D3D11_BLEND_ZERO,				//	DestBlend
	//	D3D11_BLEND_OP_ADD,				//	BlendOp
	//	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	//	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	//	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	//},	//	REPLACE
	{	//	MULTIPLY
		D3D11_BLEND_ZERO,				//	SrcBlend
		D3D11_BLEND_SRC_COLOR,			//	DestBlend
		D3D11_BLEND_OP_ADD,				//	BlendOp
		D3D11_BLEND_DEST_ALPHA,			//	SrcBlendAlpha
		D3D11_BLEND_ZERO,				//	DestBlendAlpha
		D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	MULTIPLY
	//{	//	LIGHTEN
	//	D3D11_BLEND_ONE,				//	SrcBlend
	//	D3D11_BLEND_ONE,				//	DestBlend
	//	D3D11_BLEND_OP_MAX,				//	BlendOp
	//	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	//	D3D11_BLEND_ONE,				//	DestBlendAlpha
	//	D3D11_BLEND_OP_MAX				//	BlendOpAlpha
	//},	//	LIGHTEN
	//{	//	DARKEN
	//	D3D11_BLEND_ONE,				//	SrcBlend
	//	D3D11_BLEND_ONE,				//	DestBlend
	//	D3D11_BLEND_OP_MIN,				//	BlendOp
	//	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	//	D3D11_BLEND_ONE,				//	DestBlendAlpha
	//	D3D11_BLEND_OP_MIN				//	BlendOpAlpha
	//},	//	DARKEN
	//{	//	SCREEN
	//	D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
	//	D3D11_BLEND_INV_SRC_COLOR,		//	DestBlend
	//	D3D11_BLEND_OP_ADD,				//	BlendOp
	//	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	//	D3D11_BLEND_INV_SRC_ALPHA,		//	DestBlendAlpha
	//	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	//}	//	SCREEN
};

void	blender::Init(ID3D11Device* device, ID3D11DeviceContext* context)
{
	if (bLoad)	return;			//	読み込み済みなら何度も生成しない

	HRESULT	hr = S_OK;
	for (int i = 0; i < MODE_MAX; i++)
	{
		_blend_data& _bd = blend_data[i];

		D3D11_BLEND_DESC bDesc;
		ZeroMemory(&bDesc, sizeof(bDesc));
		bDesc.AlphaToCoverageEnable = FALSE;
		bDesc.IndependentBlendEnable = FALSE;
		bDesc.RenderTarget[0].BlendEnable = (i != NONE ? TRUE : FALSE);
		bDesc.RenderTarget[0].SrcBlend = _bd.SrcBlend;
		bDesc.RenderTarget[0].DestBlend = _bd.DestBlend;
		bDesc.RenderTarget[0].BlendOp = _bd.BlendOp;
		bDesc.RenderTarget[0].SrcBlendAlpha = _bd.SrcBlendAlpha;
		bDesc.RenderTarget[0].DestBlendAlpha = _bd.DestBlendAlpha;
		bDesc.RenderTarget[0].BlendOpAlpha = _bd.BlendOpAlpha;
		bDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		hr = device->CreateBlendState(&bDesc, &blend_state[i]);
		if (FAILED(hr)) {
			assert(0);
		};
	}

	keepContext = context;
	nowMode = MODE_MAX;		//	異常値(未設定状態)

	bLoad = true;
}

HRESULT	blender::Set(BLEND_MODE mode, ID3D11DeviceContext* context)
{
	//	エラーチェック
	if (!bLoad)							return E_NOTIMPL;
	if (mode < 0 || mode >= MODE_MAX)	return E_NOTIMPL;
	if (mode == nowMode)				return E_NOTIMPL;

	//	第2引数省略時、事前に設定しているcontextを使用
	if (!context)	context = keepContext;

	//	ブレンドの設定
	context->OMSetBlendState(blend_state[mode], nullptr, 0xFFFFFFFF);
	nowMode = mode;
	return S_OK;
}

void blender::Release()
{
	for (auto &p : blend_state)
	{
		if (p)		p->Release();
		p = nullptr;
	}

	keepContext = nullptr;
	nowMode = MODE_MAX;				//	異常値(未設定状態)
	bLoad = false;
}
#endif