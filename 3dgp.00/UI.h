#pragma once
#include<DirectXMath.h>
#include <iostream>
#include <d3d11.h>
#include"sprite.h"
#include<stdio.h>
#include<string.h>
#include <vector>
#include"scene_manager.h"
class UI
{
public:
    //タイム
    int time;
    float time1;
    int time2;
    std::unique_ptr<Sprite> t_Sprite;
    std::unique_ptr<Sprite> Stagetime_Sprite;
    std::unique_ptr<Sprite> Clock;
    std::unique_ptr<Sprite> Best_Sprite;
    //お試し
    int rigit_t[100];
    int timer_r;//値保存
    int digit_Score[100];//桁の保存用配列
    int digit_[100];
    int t_digit[100];

    //セレクトスコア
    int score = 0;
    int score_render[6] = { 0 };
    std::vector<int>BestScore;

private:
    UI();
    ~UI();
public:

    void t_rigit();

    void Initialize(ID3D11Device * device);

    void Update(float elepsedtime, int p);

    void Render(ID3D11DeviceContext* immediate_context);

    void BestScoreRender(ID3D11DeviceContext* immediate_context, float pls_x, float pos_y, float size_x, float size_y, int number);

    void BestTimeRender(ID3D11DeviceContext* immediate_context, float pls_x, float pos_y, float size_x, float size_y, int number);

    void PlayerTimeRender(ID3D11DeviceContext* immediate_context);



    static UI& getInstance()
    {
        static UI  instance;
        return	instance;
    }

};


#define	pUI   UI::getInstance()
