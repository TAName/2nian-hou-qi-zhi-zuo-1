#pragma once
#include"scene.h"
#include<memory>
#include <thread>
#include <mutex>
#include"sprite.h"
#include "model.h"
#include "model_renderer.h"
#include"sound.h"

class Scene_Over :public Scene
{
    std::unique_ptr<Sprite> GameOver;
    std::unique_ptr<Sprite> black;
    std::unique_ptr<Sprite>Back_Sprite;
    std::unique_ptr<Sprite> arrow;
	std::unique_ptr<Sound>select_sound;
	std::unique_ptr<Sound>m_bgm;
    int arrow_x;
    int arrow_y;
	int select_modo;
public:
    //ゲームオーバー

    float alpha;
    bool blackF;
    void  Black(float);

    Scene_Over(ID3D11Device*device);
    int Update(float elapsed_time);
    void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
    ~Scene_Over();
};