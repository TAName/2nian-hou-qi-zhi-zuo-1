#pragma once
#include<d3d11.h>
#include <DirectXMath.h>

#include<wchar.h>
#include<vector>
#include<wrl.h>
#include <fbxsdk.h> 

class skinned_mesh
{
private:
	ID3D11VertexShader*		vertex_shader = nullptr;
	ID3D11PixelShader*		pixel_shader = nullptr;
	ID3D11InputLayout*		input_layout = nullptr;

	//ID3D11Buffer*			vertex_buffer = nullptr;
	//ID3D11Buffer*			index_buffer = nullptr;
	ID3D11Buffer*			constant_buffer = nullptr;

	ID3D11RasterizerState*	rasterizer_state = nullptr;
	ID3D11SamplerState*samplerstate = nullptr;

	int num;
public:
	struct vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 texcoord;
#define MAX_BONE_INFLUENCES 4
		FLOAT bone_weights[MAX_BONE_INFLUENCES] = { 1, 0, 0, 0 };
		INT bone_indices[MAX_BONE_INFLUENCES] = {0,0,0,0};
	};
#define MAX_BONES 32 
	struct cbuffer
	{
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4X4 world;
		DirectX::XMFLOAT4 material_color;
		DirectX::XMFLOAT4 light_direction;
		DirectX::XMFLOAT4X4 bone_transforms[MAX_BONES] = { DirectX::XMFLOAT4X4(0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f) };
	};
	struct material 
	{ 
		DirectX::XMFLOAT4 color = { 0.8f, 0.8f, 0.8f, 1.0f }; 
		ID3D11ShaderResourceView *shader_resource_view = nullptr;
		D3D11_TEXTURE2D_DESC texture2d_desc;
	};  
	//material diffuse;
	struct subset
	{
		u_int index_start = 0; // start number of index buffer 
		u_int index_count = 0; // number of vertices (indices) 
		material diffuse;
	};
	//std::vector<subset> subsets;
	struct bone
	{
		DirectX::XMFLOAT4X4 transform;
	};
	//UNIT23
	typedef std::vector<bone> skeletal;
	struct skeletal_animation : public std::vector<skeletal>
	{
		float sampling_time = 1 / 24.0f;
		float animation_tick = 0.0f;
	};

	struct mesh
	{
		Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
		std::vector<subset> subsets;
		DirectX::XMFLOAT4X4 global_transform = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
		//UNIT22
		//std::vector<skinned_mesh::bone> skeletal;
		//UNIT23
		skeletal_animation  skeletal_animation;
	};
	std::vector<mesh> meshes;
	DirectX::XMFLOAT4X4 coordinate_conversion = {
		1, 0, 0, 0,
		0, 0, 1, 0,
		0, 1, 0, 0,
		0, 0, 0, 1
	};
	skinned_mesh(ID3D11Device *device, const char *fbx_filename,bool boneflag);
	void createbuffers(ID3D11Device *device, bool boneflag);
	
	virtual ~skinned_mesh()
	{
		if (vertex_shader != nullptr)
		{
			vertex_shader->Release();
			vertex_shader = nullptr;
		}
		if (pixel_shader != nullptr)
		{
			pixel_shader->Release();
			pixel_shader = nullptr;
		}
		if (input_layout != nullptr)
		{
			input_layout->Release();
			input_layout = nullptr;
		}
		//if (vertex_buffer != nullptr)
		//{
		//	vertex_buffer->Release();
		//	vertex_buffer = nullptr;
		//}
		//if (index_buffer != nullptr)
		//{
		//	index_buffer->Release();
		//	index_buffer = nullptr;
		//}
		if (constant_buffer != nullptr)
		{
			constant_buffer->Release();
			constant_buffer = nullptr;
		}

		if (rasterizer_state != nullptr)
		{
			rasterizer_state->Release();
			rasterizer_state = nullptr;
		}
		
		meshes.clear();
	}
	void render(ID3D11DeviceContext *immediate_context,
		const DirectX::XMFLOAT4&color,
		const DirectX::XMFLOAT4& light_direction,
		const DirectX::XMFLOAT4X4&world_view_projection,
		const DirectX::XMFLOAT4X4&world,
		float elapsed_time);
};
