#include "stage_manager.h"
#include "fbx_load.h"
#include"camera.h"
#include"collision.h"
#include"vectordelete.h"
#include"camera.h"
#include"keyboad.h"
#include"UI.h"
#include"fado_out.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
#include"vector_Combo.h"
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif


//ステージ数
void Stage_Manager::Set_Stage_Max()
{
	FILE*fp;
	Stage_Max = 0;
	for (;;)
	{
		std::string FileAD = { "Data/file/stage" };
		FileAD += std::to_string(Stage_Max);
		FileAD += ".bin";
		if (fopen_s(&fp, FileAD.data(), "rb") == 0)
		{
			Stage_Max++;
			fclose(fp);
		}
		else break;
	}
	stage_select_flag = false;

}

//*************************************************************//
//初期化
//*************************************************************//
void Stage_Manager::initialize(ID3D11Device * device)
{
	create = false;

	const char* fbx_filename[] = { "Data/FBX/000_cube.fbx","Data/FBX/jumpstand.fbx","Data/FBX/go-ru.fbx"};
	for (int i = 0; i < 3; i++)
	{
		std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		FbxLoader fbx_loader;
		fbx_loader.Load(fbx_filename[i], *model_data);

		std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
		stage[i] = std::make_shared<Model>(model_resource);
	}
	const char* fbx_file_back_name[] = 
	{ 
		"Data/FBX/backobject.fbx","Data/FBX/backobject2.fbx","Data/FBX/background02_anim.fbx","Data/FBX/background03.fbx","Data/FBX/background04.fbx" ,"Data/FBX/background05.fbx"
	};
	for (int i = 0; i < BACK_OBJ; i++)
	{
		std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		FbxLoader fbx_loader;
		fbx_loader.Load(fbx_file_back_name[i], *model_data);

		backgraund[i] = std::make_shared<ModelResource>(device, std::move(model_data));
		
	}

	stage_renderer = std::make_shared<ModelRenderer>(device);
	editor_renderer = std::make_shared<ModelRenderer>(device);
	position = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	angle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	color[0] = DirectX::XMFLOAT4(1.f, 0.f, 0.f, 1.f);
	color[1] = DirectX::XMFLOAT4(0.f, 0.f, 1.f, 1.f);
	color_type = 0;
	now_Stage_No = 0;
	stage_obj_type = 0;
	Set_Stage_Max();
	if(!decision)decision = std::make_unique<Sound>("Data/sound/e.wav");
	select_sprite = std::make_unique<Sprite>(device, L"Data/image/select_box.png");
	select_back = std::make_unique<Sprite>(device, L"Data/image/select.png");
	select_font = std::make_unique<Sprite>(device, L"Data/image/stage_font.png");
	select_star = std::make_unique<Sprite>(device, L"Data/image/star.png");
	select_operation = std::make_unique<Sprite>(device, L"Data/image/key.png");
	select_tutorial = std::make_unique<Sprite>(device, L"Data/image/tutorial.png");
	de = device;
	color_obj_modo = 0;
	select_color_w = 1.f;
	select_modo = 0;
}

//セーブデータの読み込み
void Stage_Manager::Load_Stage(int stage_num)
{
	FILE*fp;
	if (fopen_s(&fp, "Data/file/color_data.bin", "rb")==0)
	{
		fread(&color[0], sizeof(DirectX::XMFLOAT4), 2, fp);
		fclose(fp);
	}
	std::vector<Save_Stage_Data>data_load;
	int size = 0;
	std::string FileAD = { "Data/file/stage" };
	FileAD += std::to_string(now_Stage_No);
	FileAD += ".bin";
	if (fopen_s(&fp, FileAD.data(), "rb") == 0)
	{
		fread(&size, sizeof(int), 1, fp);
		if (size > 0)
		{
			data_load.resize(static_cast<size_t>(size));
			fread(&data_load[0], sizeof(Save_Stage_Data), size, fp);
			for (auto&data : data_load)
			{
				s_obj.emplace_back();
				s_obj.back().initialize(stage[data.obj_type], stage_renderer);
				s_obj.back().SetPosition(data.position);
				s_obj.back().SetAngle(data.angle);
				s_obj.back().SetScale(data.scale);
				s_obj.back().SetColorType(data.color_type);
				s_obj.back().SetColor(color[data.color_type]);
				s_obj.back().SetObjType(data.obj_type);
			}
		}
		fclose(fp);
	}
	FileAD.clear();
	//背景オブジェクト
	std::vector<Save_Background_Data>data_load2;
	FileAD = { "Data/file/background" };
	FileAD += std::to_string(now_Stage_No);
	FileAD += ".bin";
	if (fopen_s(&fp, FileAD.data(), "rb") == 0)
	{
		fread(&size, sizeof(int), 1, fp);
		if (size > 0)
		{
			data_load2.resize(size);
			fread(&data_load2[0], sizeof(Save_Background_Data), size, fp);
			for (auto&data : data_load2)
			{
				b_obj.emplace_back();
				b_obj.back().initialize(backgraund[data.type], editor_renderer);
				b_obj.back().SetPosition(data.position);
				b_obj.back().SetAngle(data.angle);
				b_obj.back().SetScale(data.scale);
				b_obj.back().SetColor_W(data.color_w);
				b_obj.back().SetType(data.type);
			}
		}
		fclose(fp);
	}
}

//セーブ
void Stage_Manager::Save_Stage()
{
	FILE*fp;
	//色情報
	fopen_s(&fp, "Data/file/color_data.bin", "wb");
	fwrite(&color[0], sizeof(DirectX::XMFLOAT4), 2, fp);
	fclose(fp);

	std::vector<Save_Stage_Data>data_save;
	int size = static_cast<int>(s_obj.size());
	std::string FileAD = { "Data/file/stage" };
	FileAD += std::to_string(now_Stage_No);
	FileAD += ".bin";

	fopen_s(&fp, FileAD.data(), "wb");
	fwrite(&size, sizeof(int), 1, fp);
	if (size > 0)
	{
		for (auto&obj : s_obj)
		{
			data_save.emplace_back();
			data_save.back().position = obj.GetPosition();
			data_save.back().angle = obj.Getangle();
			data_save.back().scale = obj.GetScale();
			data_save.back().color_type = obj.GetColorType();
			data_save.back().obj_type = obj.GetObjType();
		}
		fwrite(&data_save[0], sizeof(Save_Stage_Data), size, fp);
	}
	fclose(fp);
	FileAD.clear();
	//背景オブジェクト
	FILE*ffp;
	std::vector<Save_Background_Data>data_save2;
	int size2 = static_cast<int>(b_obj.size());
	std::string FileAD2 = { "Data/file/background" };
	FileAD2 += std::to_string(now_Stage_No);
	FileAD2 += ".bin";
	fopen_s(&ffp, FileAD2.data(), "wb");

	fwrite(&size2, sizeof(int), 1, fp);

	if (size2 > 0)
	{
		for (auto&obj : b_obj)
		{
			data_save2.emplace_back();
			data_save2.back().position = obj.GetPosition();
			data_save2.back().angle = obj.GetAngle();
			data_save2.back().scale = obj.GetScale();
			data_save2.back().color_w = obj.GetColor_W();
			data_save2.back().type = obj.GetType();
		}
		fwrite(&data_save2[0], sizeof(Save_Background_Data), size, ffp);
	}
	fclose(ffp);

}

//************************************************//
//更新
//************************************************//
void Stage_Manager::Updata(float elapsed_time, int editor_modo)
{
#ifdef USE_IMGUI
	if (editor_modo == 1)
	{
		Obj_Editor();
	}
	else 
	{
		StageObjColorModo();
	}
#else
	StageObjColorModo();
#endif
	for (auto&b : b_obj)
	{
		b.Update(elapsed_time);
	}
}

//プレイヤー(操作してる人)がステージの色を変える
void Stage_Manager::StageObjColorModo()
{
	if (pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		Set_Change_Modo(color_obj_modo);
	}

}

//ステージ選択
bool Stage_Manager::Stage_Select(float eelapsed_time)
{
	if (stage_select_flag)return true;
	
#ifdef USE_IMGUI
	std::vector<std::string>StageName;
	for (int i = 0; i < Stage_Max; i++)
	{
		StageName.push_back("stage");
		StageName.back() += std::to_string(i);
	}
	ImGui::Begin("stage_select");
	ImGui::Combo("stageNo", &now_Stage_No, vector_getter, static_cast<void*>(&StageName), static_cast<int>(StageName.size()));
	stage_select_flag = ImGui::Button("start");
	if (ImGui::Button("create_stage"))
	{
		stage_select_flag = true;
		now_Stage_No = Stage_Max;
		Stage_Max++;
	}
	ImGui::End();
#else

#endif
	switch (select_modo)
	{
	case 0:
		Select_Move();
		break;
	case 1:
		pFade_out.Update(eelapsed_time);
		if (pFade_out.GetOutFlag())
		{
			stage_select_flag = true;
			select_modo = 0;
		}
		break;
	}
	if (stage_select_flag)
	{
		Load_Stage(now_Stage_No);
	}
	return false;
}
//セレクト
void Stage_Manager::Select_Move()
{
	const int before_Stage_No = now_Stage_No;
	
	if (pKeyBoad.RisingState(KeyLabel::UP))
	{
		now_Stage_No -= stage_column;
	}
	if (pKeyBoad.RisingState(KeyLabel::DOWN))
	{
		now_Stage_No += stage_column;
	}
	if (pKeyBoad.RisingState(KeyLabel::LEFT))
	{
		if(now_Stage_No%stage_column !=0)now_Stage_No--;
	}
	if (pKeyBoad.RisingState(KeyLabel::RIGHT))
	{
		if (now_Stage_No%stage_column != stage_column-1)now_Stage_No++;
	}
	if (now_Stage_No >= Stage_Max || now_Stage_No < 0)
	{
		now_Stage_No = before_Stage_No;
	}
	if (pKeyBoad.RisingState(KeyLabel::SPACE))
	{
		pFade_out.Fado_out_Start();
		select_modo = 1;
		decision->Play();
		decision->SetVolume(1.0);
	}
}
//どのオブジェクトのエディターを使うかの選択
void Stage_Manager::Obj_Editor()
{
	static const char*ObjType[] =
	{
		"stage_obj","backgraund"
	};
	Move_Set_Pos();
#ifdef USE_IMGUI
	static int obj_type = 0;
	ImGui::Begin("obj_select");
	ImGui::Combo("obj_editor_type",&obj_type, ObjType, 2);
	if (ImGui::Button("end"))
	{
		stage_select_flag = false;
		s_obj.clear();
	}
	//セーブ
	if (ImGui::Button("save"))
	{
		Save_Stage();
	}
	ImGui::End();
	switch (obj_type)
	{
	case 0:
		Stage_Obj_Editor();
		break;
	case 1:
		BackGround_Obj_Editor();
		break;
	}

#endif
}

//オブジェクトを置く場所を指定する
void Stage_Manager::Move_Set_Pos()
{
	const float speed = 3.f;
	if (pKeyBoad.PressedState(KeyLabel::UP))
	{
		position.z += speed;
	}
	if (pKeyBoad.PressedState(KeyLabel::DOWN))
	{
		position.z -= speed;
	}
	if (pKeyBoad.PressedState(KeyLabel::LEFT))
	{
		position.x += speed;
	}
	if (pKeyBoad.PressedState(KeyLabel::RIGHT))
	{
		position.x -= speed;
	}
	if (pKeyBoad.PressedState(KeyLabel::C))
	{
		position.y -= speed;
	}
	if (pKeyBoad.PressedState(KeyLabel::X))
	{
		position.y += speed;
	}

}

//StageObjのエディター
void Stage_Manager::Stage_Obj_Editor()
{
	//"floor","gimmick","goal"
#ifdef USE_IMGUI
	position.x = 0;

	static int camera_modo = 0;
	ImGui::Begin("stage editor");
	ImGui::RadioButton("stage_editor", &camera_modo, 0);
	ImGui::RadioButton("player_editor", &camera_modo, 1);
	if (camera_modo==0)	pCamera.SetCamera(position, DirectX::XMFLOAT3(-1800, 00, 0), angle, Camera::Camera_TYPE::RELATIVE_POS);
	else pCamera.SetCamera(position, DirectX::XMFLOAT3(-600, 00, 0), angle, Camera::Camera_TYPE::RELATIVE_POS);

	ImGui::Text("stageobj%d", static_cast<int>(s_obj.size()));
	static const char*ObjName[] =
	{
		"floor","gimmick","goal"
	};
	const float scale_max[]=
	{
		1000.f,100.f,100.f
	};

	if (!create)
	{
		ImGui::Combo("obj_name", &stage_obj_type, ObjName, 3);

		//生成前
		if (ImGui::Button("Ceate"))
		{
			create = true;
		}
	}
	else
	{
		//生成時のパラメーターセット
		ImGui::SliderFloat("scale.x", &scale.x, 0.f, scale_max[stage_obj_type]);
		ImGui::SliderFloat("scale.y", &scale.y, 0.f, scale_max[stage_obj_type]);
		ImGui::SliderFloat("scale.z", &scale.z, 0.f, scale_max[stage_obj_type]);
		//パラメーターセット完了
		if (ImGui::Button("End"))
		{
			switch (stage_obj_type)
			{
			case 0:
				Set_Floor();
				break;
			case 1:
				Set_Gimmick();
				break;
			case 2:
				Set_Goal();
				break;
			}
		}
	}
	//カラーデータ
	const char*color_name[] =
	{
		"colortpye0","colortpye1"
	};
	for (int i = 0; i < 2; i++)
	{
		ImGui::RadioButton(color_name[i], &color_type, i);
		if (color_type == i)
		{
			if (ImGui::SliderFloat("R", &color[color_type].x, 0.f, 1.f) ||
				ImGui::SliderFloat("G", &color[color_type].y, 0.f, 1.f) ||
				ImGui::SliderFloat("B", &color[color_type].z, 0.f, 1.f) ||
				ImGui::SliderFloat("W", &color[color_type].w, 0.f, 1.f))
			{
				for (auto&obj : s_obj)
				{
					if (obj.GetColorType() == color_type)obj.SetColor(color[color_type]);
				}
			}
		}
	}

	//消去
	if (ImGui::Button("delete"))
	{
		DirectX::XMFLOAT3 l_scale = DirectX::XMFLOAT3(scale.x*0.5f, scale.y*0.5f, scale.z*0.5f);
		DirectX::XMFLOAT3 min, max;
		min = DirectX::XMFLOAT3(position.x - l_scale.x, position.y, position.z - l_scale.z);
		max = DirectX::XMFLOAT3(position.x + l_scale.x, position.y + scale.y, position.z + l_scale.z);
		bool hit = false;
		for (auto&obj : s_obj)
		{
			hit = IsHitCube(obj.GetMin(), obj.GetMax(), min, max);
			if (hit)
			{
				obj.SetFlag(false);
				break;
			}
		}
	}

	ImGui::End();
	Vectordelete(s_obj);
#endif
}
//床のオブジェクトのセッター
void Stage_Manager::Set_Floor()
{
	DirectX::XMFLOAT3 l_scale = DirectX::XMFLOAT3(scale.x*0.5f, scale.y*0.5f, scale.z*0.5f);

	create = false;
	s_obj.emplace_back();
	s_obj.back().initialize(stage[stage_obj_type], stage_renderer);
	s_obj.back().SetAngle(angle);
	s_obj.back().SetColorType(color_type);
	s_obj.back().SetColor(color[color_type]);
	s_obj.back().SetObjType(stage_obj_type);
	s_obj.back().SetPosition(DirectX::XMFLOAT3(position.x, position.y + l_scale.y, position.z));
	s_obj.back().SetScale(l_scale);
	scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	angle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);

}
//ギミック(ジャンプ台)のオブジェクトのセッター
void Stage_Manager::Set_Gimmick()
{
	DirectX::XMFLOAT3 l_scale = DirectX::XMFLOAT3(scale.x*0.5f, scale.y*0.5f, scale.z*0.5f);
	position.y -= 10;
	create = false;
	s_obj.emplace_back();
	s_obj.back().initialize(stage[stage_obj_type], stage_renderer);
	s_obj.back().SetAngle(DirectX::XMFLOAT3(angle.x,angle.y+3.14f*1.5f,angle.z));
	s_obj.back().SetPosition(position);
	s_obj.back().SetScale2(l_scale);
	s_obj.back().SetColorType(color_type);
	s_obj.back().SetColor(color[color_type]);
	s_obj.back().SetObjType(stage_obj_type);
	scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	angle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	position.y += 10;

}
//ゴールのオブジェクトのセッター
void Stage_Manager::Set_Goal()
{
	DirectX::XMFLOAT3 l_scale = DirectX::XMFLOAT3(scale.x*0.5f, scale.y, scale.z*0.5f);

	create = false;
	s_obj.emplace_back();
	s_obj.back().initialize(stage[stage_obj_type], stage_renderer);
	s_obj.back().SetAngle(angle);
	s_obj.back().SetPosition(position);
	s_obj.back().SetScale2(l_scale);
	s_obj.back().SetColorType(color_type);
	s_obj.back().SetColor(color[color_type]);
	s_obj.back().SetObjType(stage_obj_type);
	scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	angle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);

}
//色を変える処理
void Stage_Manager::Set_Change_Modo(int color_modo)
{

	for (auto&obj : s_obj)
	{
		switch (obj.GetColorType())
		{
		case 0:

			obj.SetColor(color[1 - color_modo]);
			obj.SetModo(1 - color_modo);
			break;
		case 1:
			obj.SetColor(color[color_modo]);
			obj.SetModo(color_modo);
			break;
		}
	}	
	if (color_modo == 1)color_modo = 0;
	else color_modo = 1;
	color_obj_modo = color_modo;
}
//背景オブジェクトのエディター
void Stage_Manager::BackGround_Obj_Editor()
{
#ifdef USE_IMGUI
	pCamera.SetCamera(position, DirectX::XMFLOAT3(-600, 200, -600), angle, Camera::Camera_TYPE::RELATIVE_POS);
	static float color_w = 1.f;
	ImGui::Begin("backgraund editor");
	std::vector<std::string>backobjname;
	for (int i = 0; i < BACK_OBJ; i++)
	{
		backobjname.push_back("backgraund");
		backobjname.back() += std::to_string(i);
	}
	static int background_obj_type = 0;
	if (!create)
	{
		ImGui::Combo("obj_name", &background_obj_type, vector_getter,static_cast<void*>(&backobjname), BACK_OBJ);

		if (ImGui::Button("create"))
		{
			b_obj.emplace_back();
			b_obj.back().initialize(backgraund[background_obj_type], editor_renderer);
			b_obj.back().SetPosition(position);
			b_obj.back().SetAngle(angle);
			b_obj.back().SetScale(scale);
			b_obj.back().SetType(background_obj_type);
			create = true;
		}
		if (ImGui::Button("delete"))
		{
			for (auto&obj : b_obj)
			{
				DirectX::XMFLOAT3 leng = DirectX::XMFLOAT3(position.x - obj.GetPosition().x, position.y - obj.GetPosition().y, position.z - obj.GetPosition().z);
				float d = leng.x*leng.x + leng.y*leng.y + leng.z*leng.z;
				float size = obj.GetScale().x*obj.GetScale().x + obj.GetScale().y*obj.GetScale().y + obj.GetScale().z*obj.GetScale().z;
				if (d < size*0.1f)
				{
					obj.SetFlag(false);
					break;
				}
			}
			Vectordelete(b_obj);
		}

	}
	else
	{
		ImGui::SliderFloat("scale.x", &scale.x, 0.f, 60.f);
		ImGui::SliderFloat("scale.y", &scale.y, 0.f, 60.f);
		ImGui::SliderFloat("scale.z", &scale.z, 0.f, 60.f);
		ImGui::SliderFloat("color_w", &color_w, 0.f, 1.f);
		ImGui::SliderFloat("angle.x", &angle.x, 0.f, 3.14f*2.f);
		ImGui::SliderFloat("angle.y", &angle.y, 0.f, 3.14f*2.f);
		ImGui::SliderFloat("angle.z", &angle.z, 0.f, 3.14f*2.f);
		b_obj.back().SetColor_W(color_w);
		b_obj.back().SetPosition(position);
		b_obj.back().SetScale(scale);
		b_obj.back().SetAngle(angle);

		if (ImGui::Button("End"))
		{
			create = false;
			scale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
			angle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		}
	}
	ImGui::End();
#endif
}


//*******************************************************//
//  描画
//*******************************************************//
//ステージセレクトの描画
void Stage_Manager::Stage_Select_Render(ID3D11DeviceContext * devicecontext)
{
	DirectX::XMFLOAT2 l_pos = DirectX::XMFLOAT2(0.f, 0.f);
	static DirectX::XMFLOAT2 l_size = DirectX::XMFLOAT2(310,500);
	select_back->render(devicecontext, 0, 0, 1920, 1080, 0, 0, 1980, 1080, 0, 1, 1, 1, 1);
	static int select_timer = 0;
	DirectX::XMFLOAT4 c = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	const int star_num[] = {1,2,2,2,3};
	for (int j = 0; j < Stage_Max; j++)
	{
		if (j == now_Stage_No)
		{
			if (select_timer++ % 35 < 14)continue;
		}
		
		l_pos.x = (l_size.x + 50.f)*(j % stage_column);
		l_pos.y = (l_size.y + 50.f)*(j / stage_column);
		if (j == 0)select_tutorial->render(devicecontext, l_pos.x + 100.f, l_pos.y + 170.f, 192, 24, 0, 0, 768, 94, 0, 1, 1, 1, 1);
		select_sprite->render(devicecontext, l_pos.x+100.f, l_pos.y+200.f, l_size.x, l_size.y, 0.f, 0.f, 320.f, 510.f, 0.f, c.x, c.y, c.z, c.w);
		select_font->render(devicecontext, l_pos.x + 100.f + (l_size.x*0.7f), l_pos.y + 200.f + (l_size.y*0.02f), l_size.x*0.2f, l_size.x*0.2f, 96.f*j, 0.f, 96.f, 96.f, 0.f, c.x, c.y, c.z, c.w);
		for (int i = 0; i < star_num[j]; i++)
		{
			select_star->render(devicecontext, l_pos.x + 110.f + ((l_size.x*0.2f)*i), l_pos.y + 280.f, l_size.x*0.2f, l_size.x*0.2f, 0, 0, 96, 96, 0, 1, 1, 1, 1);
		}
        pUI.BestScoreRender(devicecontext, l_pos.x + 100.f, l_pos.y + 200.f, l_size.x, l_size.y, j);
        pUI.BestTimeRender(devicecontext, l_pos.x + 100.f, l_pos.y + 200.f, l_size.x, l_size.y, j);
    }
	select_operation->render(devicecontext, 300, 800, 700, 48, 0, 0, 1400, 96, 0, 1, 1, 1, 1);
	pFade_out.Render(devicecontext);
}

//通常描画
void Stage_Manager::Render(float elapsed_time, ID3D11DeviceContext * devicecontext, const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4 & light)
{
#ifdef USE_IMGUI
	DirectX::XMMATRIX W;


	DirectX::XMFLOAT3 l_scale = DirectX::XMFLOAT3(scale.x*0.5f, scale.y*0.5f, scale.z*0.5f);
	DirectX::XMFLOAT4 color_set = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 0.5f);
	if (create)
	{
		color_set = DirectX::XMFLOAT4(color[color_type].x, color[color_type].y, color[color_type].z, 0.5f);
	}
	DirectX::XMMATRIX s, r, t;
	s = DirectX::XMMatrixScaling(l_scale.x, l_scale.y, l_scale.z);//スケール行列
	r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
	t = DirectX::XMMatrixTranslation(position.x, position.y + l_scale.y, position.z);//移動行列

	W = s*r*t;



	stage[0]->CalculateLocalTransform();
	stage[0]->CalculateWorldTransform(W);
	stage_renderer->Begin(devicecontext,
		world_view_projection,
		light	// ライトの向き
	);
	stage_renderer->Render(devicecontext, *stage[0], color_set);
	stage_renderer->End(devicecontext);
#endif
	int i = 0;
	for (auto&obj : s_obj)
	{
		i++;
		if (obj.GetMin().z > -pCamera.GetFocus().z + 1200)continue;
		if (obj.GetMax().z < -pCamera.GetFocus().z - 400)continue;
		obj.Render(elapsed_time, devicecontext, world_view_projection, light);
	}
	for (auto&b : b_obj)
	{
		if (b.GetPosition().z > -pCamera.GetFocus().z + 2400)continue;
		if (b.GetPosition().z < -pCamera.GetFocus().z - 600)continue;

		b.Render(elapsed_time, devicecontext, world_view_projection, light);
	}
}
