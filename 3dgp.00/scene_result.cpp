#include "scene_result.h"
#include "fbx_load.h"
#include"camera.h"
#include "Ranking.h"
#include "UI.h"
#include"keyboad.h"
#include "stage_manager.h"
#include"fado_out.h"
Scene_Result::Scene_Result(ID3D11Device * device)
{
	select_sound = std::make_unique<Sound>("Data/sound/e.wav");
	m_bgm = std::make_unique<Sound>("Data/sound/calculator.wav");
  
    pRanking.Initialize(device);
	pFade_out.initialize(device);
	select_modo = 0;
	m_bgm->Play(true);
	m_bgm->SetVolume(1.0);

}

int Scene_Result::Update(float elapsed_time)
{
	//model->UpdateAnimation(1.0f / 60.0f);
    if (pRanking.arrow_x == 1089 && pKeyBoad.RisingState(KeyLabel::SPACE))
    {
        pStage_Manager.SetStage_Select(false);
		select_modo = 2;
		select_sound->Play();
		select_sound->SetVolume(1.0);
		pFade_out.Fado_out_Start();
    }
    if (pRanking.arrow_x == 1610 && pKeyBoad.RisingState(KeyLabel::SPACE))
    {
		select_modo = 1;
		select_sound->Play();
		select_sound->SetVolume(1.0);
		pFade_out.Fado_out_Start();
    }
	if (select_modo > 0)
	{
		pFade_out.Update(elapsed_time);
		if (pFade_out.GetOutFlag())return select_modo;
	}
	else
	{
		pRanking.Update(elapsed_time);
	}
	return 0;
   
}

void Scene_Result::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{

    const float PI = 3.14f;
    //ワールド行列を作成
    DirectX::XMMATRIX W;
    {
        static float rz = 0;
        //rz += 0.2f*(PI / 180.f);
        static float rt = (PI / 180.f)*45.f;
        //rt += 0.2f*(PI / 180.f);
        //Z軸方向に前後移動してみる
        static float mz = 0;
        //mz += 0.002f;
        float tz = sinf(mz)*5.0f;

        DirectX::XMFLOAT3 scale(1, 1, 1);
        DirectX::XMFLOAT3 rotate(0, 0, rz);
        DirectX::XMFLOAT3 translate(0, 0, 0);

        DirectX::XMMATRIX s, r, t;
        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);//回転行列
        t = DirectX::XMMatrixTranslation(translate.x, translate.y, translate.z);//移動行列

        W = s*r*t;
    }
    //ビュー行列
    DirectX::XMMATRIX V;
    {
        //カメラの設定
        DirectX::XMVECTOR eye, focus, up;
        eye = DirectX::XMVectorSet(10.0f, 50.0f, -500.0f, 1.0f);//視点
        focus = DirectX::XMVectorSet(0.0f, 50.0f, 0.0f, 1.0f);//注視点
        up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);//上ベクトル

        V = DirectX::XMMatrixLookAtLH(eye, focus, up);
    }
    DirectX::XMMATRIX P;
    {
        //画面サイズ取得のためビューポートを取得
        D3D11_VIEWPORT viewport;
        UINT num_viewports = 1;
        devicecontext->RSGetViewports(&num_viewports, &viewport);

        //角度をラジアン(θ)に変換
        float fov_y = 30 * (PI / 180.f);//角度
        float aspect = viewport.Width / viewport.Height;//画面比率
        float near_z = 0.1f;//表示最近面までの距離
        float far_z = 1000.0f;//表紙最遠面までの距離

        P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
    }
    //
    //立方体の描画
    DirectX::XMFLOAT4X4 world_view_projection;
    {
        DirectX::XMMATRIX WVP;
        DirectX::XMMATRIX C = DirectX::XMMatrixSet(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, -1, 0,
            0, 0, 0, 1
            );

        WVP = C*V*P;
        DirectX::XMStoreFloat4x4(&world_view_projection, WVP);
    }

    ////立方体の描画
    //model->CalculateLocalTransform();
    //model->CalculateWorldTransform(W);
    //model_render->Begin(devicecontext,
    //	world_view_projection,
    //	DirectX::XMFLOAT4(0, -1, -1, 0)	// ライトの向き
    //);
    //model_render->Render(devicecontext, *model,DirectX::XMFLOAT4(1.f,1.f,1.f,1.f));
    //model_render->End(devicecontext);
    pRanking.Render(devicecontext, elapsed_time);
	pFade_out.Render(devicecontext);
}

Scene_Result::~Scene_Result()
{
	if (m_bgm)m_bgm->Pause();
}
