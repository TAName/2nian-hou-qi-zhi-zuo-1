#pragma once
#include "model.h"
#include "model_renderer.h"
#include<memory>

class BackgroundObj
{
	std::unique_ptr<Model>m_model;
	std::shared_ptr<ModelRenderer>m_model_renderer;
	DirectX::XMFLOAT3 m_position;
	DirectX::XMFLOAT3 m_scale;
	DirectX::XMFLOAT3 m_angle;
	DirectX::XMFLOAT4 m_color;
	bool flag;
	int m_type;
public:
	operator const bool() const { return flag; };
	void SetFlag(bool f) { flag = f; }
	void initialize(std::shared_ptr<ModelResource>model_resource, std::shared_ptr<ModelRenderer>model_renderer);
	//セッター
	void SetPosition(const DirectX::XMFLOAT3&position) { m_position = position; };
	void SetAngle(const DirectX::XMFLOAT3&angle) { m_angle = angle; };
	void SetScale(const DirectX::XMFLOAT3&scale) { m_scale = scale; };
	void SetColor_W(const float color_w) { m_color.w = color_w; }
	void SetType(const int type) { m_type = type; }
	//ゲッター
	const DirectX::XMFLOAT3&GetPosition() { return m_position; }
	const DirectX::XMFLOAT3&GetAngle() { return m_angle; }
	const DirectX::XMFLOAT3&GetScale() { return m_scale; }
	const float&GetColor_W() { return m_color.w; }
	const int&GetType() { return m_type; }
	void Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext, const DirectX::XMFLOAT4X4&world_view_projection, const DirectX::XMFLOAT4&light);
};