#pragma once
#include <d3d11.h>
#include <directxmath.h>
#include<stdlib.h>
#include<wchar.h>
#include<vector>
#include<wrl.h>
class static_mesh
{
private:
	ID3D11VertexShader*		vertex_shader = nullptr;
	ID3D11PixelShader*		pixel_shader = nullptr;
	ID3D11InputLayout*		input_layout = nullptr;

	ID3D11Buffer*			vertex_buffer = nullptr;
	ID3D11Buffer*			index_buffer = nullptr;
	ID3D11Buffer*			constant_buffer = nullptr;

	ID3D11RasterizerState*	rasterizer_state = nullptr;

	ID3D11ShaderResourceView*srview;
	D3D11_TEXTURE2D_DESC texture2d;
	ID3D11SamplerState*samplerstate;

	u_int vertex_num;
public:
	struct vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 texcoord;

	};
	struct cbuffer
	{
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4X4 world;
		DirectX::XMFLOAT4 material_color;
		DirectX::XMFLOAT4 light_direction;
	};
	//struct subset 
	//{
	//	std::wstring usemtl;    
	//	u_int index_start = 0;  // start number of index buffer   
	//	u_int index_count = 0;  // number of vertices (indices)   
	//};
	struct subset
	{
		std::wstring usemtl;
		u_int mat_index = 0;
		u_int index_start = 0;
		u_int index_count = 0;
	};	
	std::vector<subset>subsets;
	struct material {
		std::wstring newmtl;   
		DirectX::XMFLOAT3 Ka = { 0.2f, 0.2f, 0.2f };    
		DirectX::XMFLOAT3 Kd = { 0.8f, 0.8f, 0.8f };    
		DirectX::XMFLOAT3 Ks = { 1.0f, 1.0f, 1.0f };    
		u_int illum = 1;    
		std::wstring map_Kd;    
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;//スマートポインタ?????
		D3D11_TEXTURE2D_DESC texture2d_desc;
		//省略（必要に応じてメンバ変数を追加する）     ：   
	};  
	std::vector<material>materials; 
	
	DirectX::XMFLOAT3 Diffuse_color;
	static_mesh(ID3D11Device *device, wchar_t*,bool);
	virtual ~static_mesh()
	{
		if (vertex_shader != nullptr)
		{
			vertex_shader->Release();
			vertex_shader = nullptr;
		}
		if (pixel_shader != nullptr)
		{
			pixel_shader->Release();
			pixel_shader = nullptr;
		}
		if (input_layout != nullptr)
		{
			input_layout->Release();
			input_layout = nullptr;
		}
		if (vertex_buffer != nullptr)
		{
			vertex_buffer->Release();
			vertex_buffer = nullptr;
		}
		if (index_buffer != nullptr)
		{
			index_buffer->Release();
			index_buffer = nullptr;
		}
		if (constant_buffer != nullptr)
		{
			constant_buffer->Release();
			constant_buffer = nullptr;
		}

		if (rasterizer_state != nullptr)
		{
			rasterizer_state->Release();
			rasterizer_state = nullptr;
		}
		if (srview != nullptr)
		{
			srview->Release();
			srview = nullptr;
		}
		if (samplerstate != nullptr)
		{
			samplerstate->Release();
			samplerstate = nullptr;
		}
	}
	void render(ID3D11DeviceContext *immediate_context,
		const DirectX::XMFLOAT4&color,
		const DirectX::XMFLOAT4& light_direction,
		const DirectX::XMFLOAT4X4&world_view_projection,
		const DirectX::XMFLOAT4X4&world) const;
};
