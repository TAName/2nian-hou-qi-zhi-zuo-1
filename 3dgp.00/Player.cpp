#include "fbx_load.h"
#include "Player.h"
#include"stage_manager.h"
#include"collision.h"
#include"particle.h"
#include"goal_production.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Player::Player(ID3D11Device*device)
{
    speed = DirectX::XMFLOAT2(0,0);
	pos = DirectX::XMFLOAT3(0, 10, 0);
	angle = DirectX::XMFLOAT3(0, 0, 0);
	scale = DirectX::XMFLOAT3(10,10,10);
    //ジャンプ
    Jflag = false;
    parameter_data.Gravity = 3.0f;
	parameter_data.Jpower = 20.f;
	parameter_data.speed_x = 6.f;
    //Model
    const char* fbx_filename = "Data/FBX/new_player_anim.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(fbx_filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    play = std::make_shared<Model>(model_resource);
    player_render = std::make_shared<ModelRenderer>(device);
    play->PlayAnimation(0, true);
	floor = false;
	Load_Parameter();
	G = Gravity;
	exist = true;
	sphere = std::make_unique<geometric_Cylinder>(device, 32);
	pattrn = 0;
	lispon_timer = 0;
	//m_run = std::make_unique<Sound>("Data/sound/des.wav");
}

Player::~Player()
{
	//m_run->Pause();
}

void Player::Update(int editor_modo)
{
	static float anim_time = 1.f / 30.f;
	static const float shadow = 1.f;
	before_pos = pos;
	color_w = 1.f;
	if (editor_modo > 0)
	{
		pos = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
		cy_pos.x = pos.x;
		cy_pos.y = pos.y + shadow;
		cy_pos.z = pos.z;
		Jflag = false;
		play->PlayAnimation(0, true);
		anim_time = 1.f / 30.f;
		exist = true;
		pattrn = 0;
		return;
	}
	

	speed = DirectX::XMFLOAT2(0.f, 0.f);
	Jump();
	speed.x = parameter_data.speed_x;
	if (!gravity_flag)
	{
		G ++;
		speed.y -= G;
	}
	else
	{
		G = Gravity;
	}
	pos.z += speed.x;
	pos.y += speed.y;
	cy_pos.x = pos.x;
	cy_pos.y = pos.y+ shadow;
	cy_pos.z = pos.z;
	//*****************当たり判定***************************//
	now_pos = pos;
	min.x = now_pos.x - 17.5f;
	max.x = now_pos.x + 17.5f;
	if (before_pos.y < now_pos.y)
	{
		min.y = before_pos.y; 
		max.y = now_pos.y + 66.f;
	}
	else 
	{
		min.y = now_pos.y;
		max.y = before_pos.y + 66.f;
	}
	if (before_pos.z < now_pos.z) {
		min.z = before_pos.z - 11.5f;
		max.z = now_pos.z + 11.5f;
	}
	else
	{
		min.z = now_pos.z - 11.5f;
		max.z = before_pos.z + 11.5f;
	}
	//当たり判定
	judge(anim_time,shadow);

	play->UpdateAnimation(anim_time);
}
//当たり判定
void Player::judge(float&anim_time, float shadow)
{
	gravity_flag = false;
	bool hit = false;
	float p_cy = 0.f;
	for (auto&obj : pStage_Manager.GetStageObjs())
	{
		if (obj.GetModo() == 1)continue;
		//地面の当たり判定
		hit = IsHitCube(obj.GetMin(), obj.GetMax(), min, max);
		if (hit)
		{
			switch (obj.GetObjType())
			{
			case 0:
				judge_floor(obj.GetMin(), obj.GetMax(), anim_time);
				break;
			case 1:
				judge_gimmick(obj.GetMin(), obj.GetMax());
				break;
			case 2:
				judge_goal(obj.GetMin(), obj.GetMax());
				break;
			}
		}
		if (obj.GetObjType() != 0)continue;
		if (gravity_flag && !Jflag)
		{
			DirectX::XMFLOAT3 jump_min, jump_max;
			jump_max = DirectX::XMFLOAT3(obj.GetMax().x, obj.GetMax().y+30.f, obj.GetMax().z);
			jump_min = DirectX::XMFLOAT3(obj.GetMin().x, obj.GetMax().y, obj.GetMax().z - 10.f);
			hit = IsHitCube(jump_min, jump_max, min, max);
			if (hit)
			{
				Jflag = true;
				Jpower = parameter_data.Jpower;
				play->PlayAnimation(2, true);
				anim_time = 1.f / 15.f;
			}
		}
		if (!gravity_flag)
		{
			hit = IsHitCube(DirectX::XMFLOAT3(obj.GetMin().x, 0.f, obj.GetMin().z), DirectX::XMFLOAT3(obj.GetMax().x, 0.f, obj.GetMax().z), DirectX::XMFLOAT3(min.x, 0.f, min.z), DirectX::XMFLOAT3(max.x, 0.f, max.z));
			if (hit)
			{
				if (p_cy != 0.f)
				{
					float pcy = obj.GetMax().y - pos.y;
					if (p_cy*p_cy>pcy*pcy)cy_pos.y = obj.GetMax().y + shadow;
				}
				else
				{
					cy_pos.y = obj.GetMax().y + shadow;
					p_cy = cy_pos.y - pos.y;
				}
				color_w = 1.f*(1.f - (1.f / p_cy));
			}
		}
	}
	if (p_cy == 0.f && !gravity_flag)
	{
		cy_pos.y = pos.y - 600.f;
		color_w = 0.f;
	}
	const float pos_y_min = -600.f;
	if (pos.y < pos_y_min&&exist)
	{
		exist = false;
		anim_time = 1.f / 30.f;
	}
	if (!exist)
	{
		Lispon();
	}

}

void Player::judge_floor(const XMFLOAT3 & obj_min, const XMFLOAT3 & obj_max, float&anim_time)
{
	if (before_pos.z + 11.5f <= obj_min.z&&now_pos.z + 11.5f >= obj_min.z)
	{
		//床の前面に当たってるとき
		speed.x = 0;
		pos.z = obj_min.z - 11.5f;

	}
	else if (before_pos.y >= obj_max.y&&now_pos.y <= obj_max.y)
	{
		//床の上面に当たってるとき

		speed.y = 0;
		pos.y = obj_max.y;
		if (Jflag)
		{
			play->PlayAnimation(0, true);
			anim_time = 1.f / 30.f;
			pParticle_Manager.Set_particle(3, 30, pos);
		}
		 
		else pParticle_Manager.Set_particle(0, 5, pos);
		gravity_flag = true;
		Jflag = false;
	}
	else if (before_pos.y <= obj_min.y&&now_pos.y >= obj_min.y)
	{
		//床の下面に当たってるとき
		pos.y = obj_min.y - 66.f;
		Jpower = 0.f;
	}

}

void Player::judge_gimmick(const XMFLOAT3 & obj_min, const XMFLOAT3 & obj_max)
{
	if (before_pos.z + 11.5f <= obj_min.z&&now_pos.z + 11.5f >= obj_min.z)
	{
		//床の前面に当たってるとき
		Jflag = true;
		Jpower = parameter_data.Jpower*2.f;
		play->PlayAnimation(2, true);
	}

}

void Player::judge_goal(const XMFLOAT3 & obj_min, const XMFLOAT3 & obj_max)
{
	pGoal_Production.SetGoalFlag(true);
	pGoal_Production.SetPlayer_Pos(pos);
}



void Player::Render(ID3D11DeviceContext* context, float elapsed_time, const DirectX::XMFLOAT4X4&WVP, const DirectX::XMFLOAT4&light_direction)
{
	if (!exist)return;
    const float PI = 3.14f;
    //ワールド行列を作成
    DirectX::XMMATRIX W;
    {
      
        DirectX::XMMATRIX s, r, t;
        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
        t = DirectX::XMMatrixTranslation(pos.x,pos.y,pos.z);//移動行列

        W = s*r*t;
    }


    //立方体の描画
        play->CalculateLocalTransform();
        play->CalculateWorldTransform(W);
        player_render->Begin(context,
        WVP,
        light_direction	// ライトの向き
        );
        player_render->Render(context, *play, DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f));
        player_render->End(context);
}

void Player::Render_Shadow(ID3D11DeviceContext * context, const DirectX::XMFLOAT4X4 & view, const DirectX::XMFLOAT4X4 & projection, const DirectX::XMFLOAT4 & light_direction)
{
	DirectX::XMFLOAT4X4 world,worldviewprojection;
	{
		DirectX::XMMATRIX s, r, t,W;
		s = DirectX::XMMatrixScaling(30.f, 0.01f, -1.f*70.f);//スケール行列
		r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
		t = DirectX::XMMatrixTranslation(cy_pos.x, cy_pos.y, -cy_pos.z);//移動行列

		W = s*r*t;
		DirectX::XMStoreFloat4x4(&world, W);
		DirectX::XMStoreFloat4x4(&worldviewprojection, W*DirectX::XMLoadFloat4x4(&view)*DirectX::XMLoadFloat4x4(&projection));
	}

	sphere->render(context, DirectX::XMFLOAT4(0.f, 0.f, 0.f, color_w), light_direction, worldviewprojection, world);

}


void Player::Jump()
{
	if (Jflag)
	{
		speed.y = Jpower;
		if (Jpower > 0.f)
			Jpower--;
	}

}
void Player::Lispon()
{
	
	static DirectX::XMFLOAT3 endpos;
	switch (pattrn)
	{
	case 0:
		endpos = pos;
		pattrn++;
		pParticle_Manager.Set_particle(2, 30, endpos);
		break;
	case 1:
		pos = endpos;
		if (lispon_timer++ > 20)
		{
			lispon_timer = 0;
			pattrn++;
		}
		break;
	case 2:
		pos = DirectX::XMFLOAT3(0, 20, 0);
		play->PlayAnimation(0, true);
		Jflag = false;
		speed.y = 0;
		G = Gravity;
		pStage_Manager.Set_Change_Modo(1);
		pattrn = 0;
		exist = true;
		break;
	}

}
//***************************************************************************************************//
//パラメーターセットエディター
//***************************************************************************************************//
//セーブ
void Player::Save_Parameter()
{
	FILE*fp;
	fopen_s(&fp, "Data/file/player.bin", "wb");
	fwrite(&parameter_data, sizeof(Parameter_Data), 1, fp);
	fclose(fp);
}
//ロード
void Player::Load_Parameter()
{
	FILE*fp;
	if (fopen_s(&fp, "Data/file/player.bin", "rb")==0)
	{
		fread(&parameter_data, sizeof(Parameter_Data), 1, fp);
		fclose(fp);
	}
	Gravity = parameter_data.Gravity;
}
//セット
void Player::Parameter_Editor(int editor_modo)
{
	if (editor_modo != 2)return;
#ifdef USE_IMGUI
	ImGui::Begin("player_parameter");
	ImGui::SliderFloat("speed_x", &parameter_data.speed_x, 0.f, 30.f);
	ImGui::SliderFloat("Jpower", &parameter_data.Jpower, 0.f, 30.f);
	if (ImGui::SliderFloat("Gravity", &parameter_data.Gravity, 0.f, 30.f))
	{
		Gravity = parameter_data.Gravity;
	}
	if (ImGui::Button("save"))
	{
		Save_Parameter();
	}
	ImGui::End();
#endif
}
