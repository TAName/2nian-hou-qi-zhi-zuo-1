#pragma once
#include"Geometric_Primitive.h"
#include"Board.h"
#include<memory>
#include<vector>
struct Particle_Data
{
	DirectX::XMFLOAT3 angle;
	DirectX::XMFLOAT3 scale;
	DirectX::XMFLOAT3 speed;
	DirectX::XMFLOAT3 accel;
	DirectX::XMFLOAT3 move_angle;
	DirectX::XMFLOAT3 variation_pos;
	DirectX::XMFLOAT3 variation_speed;
	DirectX::XMFLOAT4 color;
	int type;
	int timer_max;
};
class Particle
{
	std::unique_ptr<Board>boad;
	std::shared_ptr<geometric_primitive>m_obj;
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 angle;
	DirectX::XMFLOAT3 scale;
	DirectX::XMFLOAT3 speed;
	DirectX::XMFLOAT3 accel;
	DirectX::XMFLOAT4 color;
	DirectX::XMFLOAT3 move_angle;
	int type;
	int timer;
	float timer_split;
	bool flag;
	float start_color_w;
	DirectX::XMFLOAT4X4 world;
public:
	operator const bool() const { return flag; };
	Particle(std::shared_ptr<geometric_primitive>obj, const Particle_Data&data, const DirectX::XMFLOAT3&pos);
	void Update(float elapsed_time);
	void Move0();
	void Move1(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection, const DirectX::XMFLOAT4&light);
};
class Particle_Manager
{
	std::vector<Particle>particles;
	std::shared_ptr<geometric_primitive>obj;
	std::vector<Particle_Data>data;
	int m_number;
	int type;
public:
	void initialize(ID3D11Device*device);
	void Load_Data();
	void Save_Data();
	void Set_particle(int type,int num, const DirectX::XMFLOAT3&pos);
	void Update(float elapsed_time);
	void Editor(int editor_modo);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection, const DirectX::XMFLOAT4&light);
	void Relese() { particles.clear(); }
	static Particle_Manager&getinctance()
	{
		static Particle_Manager particle_manager;
		return particle_manager;
	}
};
#define pParticle_Manager (Particle_Manager::getinctance())