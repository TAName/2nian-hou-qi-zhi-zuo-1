#include "fado_out.h"

void Fade_out::initialize(ID3D11Device * device)
{
	back_out = std::make_unique<Sprite>(device, L"Data/image/siro.png");
	color_w = 0.f;
	out_flag = true;
}

void Fade_out::Update(float eelapsed_time)
{
	if (out_flag)return;
	color_w += eelapsed_time;
	if (color_w >= 1)out_flag = true;
}

void Fade_out::Render(ID3D11DeviceContext * devicecontext)
{
	back_out->render(devicecontext, 0.f, 0.f, 1980.f, 1080.f, 0.f, 0.f, 1024.f, 1024.f, 0.f, 0.f, 0.f, 0.f, color_w);
}
