#include "goal_production.h"
#include"stage_manager.h"
#include"particle.h"
#include"fado_out.h"
void Goal_Production::initialize(ID3D11Device * device)
{
	goal = std::make_unique<Sprite>(device, L"Data/image/Finish.png");
	line = std::make_unique<Sprite>(device, L"Data/image/focus_line_anim.png");
	back_out = std::make_unique<Sprite>(device,L"Data/image/siro.png");
	color_w = 0.f;
	timer = 0;
	timer_count = 5;
	goal_flag = false;
	font_move = 2.1f;
	particle_flag = false;
}

void Goal_Production::Update(float elapsed_time)
{
	if (!particle_flag)
	{
		pParticle_Manager.Set_particle(2, 30, player_pos);
		particle_flag = true;
		pFade_out.Fado_out_Start();
	}
	if(font_move>1.5f)font_move -= 0.1f;
	pFade_out.Update(elapsed_time);
	if (pFade_out.GetOutFlag())
	{
		pStage_Manager.SetChangeStage(3);
		return;
	}
}

void Goal_Production::Render(ID3D11DeviceContext * devicecontext)
{
	if (!goal_flag)return;
	float x = 1920.f, y = 1080.f;
	static float anim = 0;
	if (timer%timer_count == 0)
	{
		anim++;
		if (anim >= 3)anim = 0;
	}
	line->render(devicecontext, 0.f, 0.f, 1980.f, 1080.f, x*anim, 0, x, y, 0.f, 1.f, 1.f, 1.f, 1.f);
	goal->render(devicecontext, 650.f-(100.f*font_move), 550.f - (100.f*font_move), 600.f*font_move, 96.f*font_move, 0.f, 0.f, 600.f, 96.f, 0.f, 1.f, 1.f, 1.f, 1.f);
	pFade_out.Render(devicecontext);
	//back_out->render(devicecontext, 0.f, 0.f, 1980.f, 1080.f, 0.f, 0.f, 1024.f, 1024.f, 0.f, 0.f, 0.f, 0.f,  color_w);
}
